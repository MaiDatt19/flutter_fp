import 'package:cloud_firestore/cloud_firestore.dart';

Stream<DocumentSnapshot<Object?>>? getUserAsStream(id) {
  return FirebaseFirestore.instance.collection("users").doc(id).snapshots();
}
