import 'package:cloud_firestore/cloud_firestore.dart';

final firestore = FirebaseFirestore.instance;
void updateDocumentById(String collectionPath, String docId, data) {
  firestore.collection(collectionPath).doc(docId).update(data);
}

Stream<DocumentSnapshot<Object?>>? getDocumentByIdAsStream(
    String collectionPath, String docId) {
  return firestore.collection(collectionPath).doc(docId).snapshots();
}

void createDocumentAutoId(String collectionPath, data) {
  firestore.collection(collectionPath).add(data);
}

void createDocumentWithId(String collectionPath, id, data) {
  firestore.collection(collectionPath).doc(id).set(data);
}
