import 'package:cloud_firestore/cloud_firestore.dart';

Stream<DocumentSnapshot<Object?>>? getShopAsStream(id) {
  return FirebaseFirestore.instance.collection("shops").doc(id).snapshots();
}

void updateShop(form) {}
