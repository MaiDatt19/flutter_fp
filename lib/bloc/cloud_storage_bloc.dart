import 'package:firebase_storage/firebase_storage.dart';

final storageRef = FirebaseStorage.instance.ref();
void uploadFileToFireStore(file, String path) {
  storageRef.child(path).putFile(file);
}

// final downloadImage
Stream getFileAsStream(String path, file) {
  return storageRef.child(path).writeToFile(file).snapshotEvents;
}

Stream<String> getDownloadUrlAsStream(String path) {
  return storageRef.child(path).getDownloadURL().asStream();
}
