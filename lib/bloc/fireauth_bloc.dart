import 'package:firebase_auth/firebase_auth.dart';

bool isSignedIn() {
  if (FirebaseAuth.instance.currentUser != null) {
    return true;
  } else {
    return false;
  }
}

String getUid() {
  if (isSignedIn()) {
    return FirebaseAuth.instance.currentUser!.uid;
  } else {
    return "";
  }
}
