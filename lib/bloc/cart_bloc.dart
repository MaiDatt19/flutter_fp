// import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_elegant_number_button/flutter_elegant_number_button.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/price_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/main_pages/place_order.dart';
import 'package:flutter_fp/ui/main_pages/product_detail.dart';
import 'package:flutter_fp/ui/style/text_style.dart';
// import '../../ui/style/colors.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:page_transition/page_transition.dart';

import '../ui/style/colors.dart';

StreamBuilder getCartItems(context) {
  // if (ordersRef == null) {
  //   print("oke null");
  // }
  var sumCalculateBloc = SumCalculateBloc();
  return StreamBuilder<QuerySnapshot>(
    stream: FirebaseFirestore.instance
        .collection('cartItems')
        .where("buyerId", isEqualTo: getUid())
        .snapshots(),
    builder:
        (BuildContext context, AsyncSnapshot<QuerySnapshot> orderSnapshot) {
      if (orderSnapshot.hasData && orderSnapshot.data!.docs.isNotEmpty) {
        // double total = 0;
        List<double> sums = [];
        return Stack(
          // direction: Axis.vertical,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 35),
              child: ListView(
                // shrinkWrap: true,
                children: orderSnapshot.data!.docs
                    .map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;
                      String productId = data['productId'];
                      return FutureBuilder<DocumentSnapshot>(
                          future: FirebaseFirestore.instance
                              .collection("products")
                              .doc(productId)
                              .get(),
                          builder: (BuildContext context,
                              AsyncSnapshot<DocumentSnapshot> productSnapshot) {
                            if (productSnapshot.hasData) {
                              double sum = data['quantity'] *
                                  productSnapshot.data!.get('price');

                              sums.add(sum);
                              sumCalculateBloc.priceCalculate.add(sums);
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          child: ProductDetailScreen(
                                              id: productId),
                                          type:
                                              PageTransitionType.rightToLeft));
                                },
                                child: Slidable(
                                  child: Card(
                                    elevation: 5,
                                    shadowColor: secondaryColor,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 6),
                                    child: Container(
                                      padding: const EdgeInsets.all(10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Expanded(
                                              child: AspectRatio(
                                            aspectRatio: 0.5,
                                            child: FutureBuilder(
                                              future: FirebaseStorage.instance
                                                  .ref()
                                                  .child("images")
                                                  .child(productId)
                                                  .child("0")
                                                  .getDownloadURL(),
                                              builder: (BuildContext context,
                                                  snapshot) {
                                                if (snapshot.hasData) {
                                                  // return Text(snapshot.data.toString());
                                                  return Image(
                                                      image: NetworkImage(
                                                          snapshot.data
                                                              .toString()));
                                                } else {
                                                  // return const Text("loading");
                                                  return Container();
                                                  // return const Image(
                                                  //     image: AssetImage(
                                                  //         "assets/loading.png"));
                                                }
                                              },
                                            ),
                                          )),
                                          Expanded(
                                              flex: 6,
                                              child: Container(
                                                  padding:
                                                      const EdgeInsets.all(8),
                                                  child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 20),
                                                        ),
                                                        Text(
                                                          productSnapshot.data!
                                                              .get('name'),
                                                        ),
                                                        Text(
                                                          "\$ " +
                                                              sum.toString(),
                                                          style:
                                                              getSmallProfileNameStyle(),
                                                        )
                                                      ]))),
                                          Center(
                                            child: ElegantNumberButton(
                                              initialValue: data['quantity'],
                                              minValue: 1,
                                              maxValue: 9999999,
                                              step: 1,

                                              // buttonSizeHeight: 30,
                                              // buttonSizeWidth: 30,
                                              decimalPlaces: 0,
                                              color: secondaryColor,
                                              onChanged: (value) async {
                                                // setState(() {
                                                //   quantity = value;
                                                // });
                                                // quantity = value.;
                                                FirebaseFirestore.instance
                                                    .collection("cartItems")
                                                    .doc(document.id)
                                                    .update(
                                                        {'quantity': value});
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  endActionPane: ActionPane(
                                      motion: const ScrollMotion(),
                                      children: [
                                        SlidableAction(
                                          flex: 2,
                                          onPressed: (context) {
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    child: ProductDetailScreen(
                                                        id: productId),
                                                    type: PageTransitionType
                                                        .rightToLeft));
                                          },
                                          backgroundColor: Colors.white,
                                          foregroundColor: primaryColor,
                                          icon: Icons.remove_red_eye,
                                          label: 'VIEW',
                                        ),
                                        SlidableAction(
                                          onPressed: (context) {
                                            FirebaseFirestore.instance
                                                .collection('cartItems')
                                                .doc(document.id)
                                                .delete();
                                          },
                                          backgroundColor: const Color.fromARGB(
                                              255, 239, 209, 209),
                                          foregroundColor: Colors.red,
                                          icon: Icons.delete_outline,
                                        )
                                      ]),
                                ),
                              );
                            } else {
                              return Center(
                                child: getCircularProgressBar(),
                              );
                            }
                          });
                      // return ListTile(
                      //   title: Text(data['productId']),
                      //   subtitle: Text(data['buyerId'].toString()),
                      // );
                    })
                    .toList()
                    .cast(),
              ),
            ),
            StreamBuilder<double?>(
                stream: sumCalculateBloc.resultStream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.bottomCenter,
                      child: getCustomButton(
                          "CheckOut | \$" + snapshot.data!.toString(), () {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: PlaceOrderScreen(),
                                type: PageTransitionType.bottomToTop));
                      }, primaryColor, tertiaryColor, 2),
                    );
                  } else {
                    return Text("");
                  }
                })
          ],
        );
      } else {
        return const Center(
          child: Text("No orders"),
        );
      }
    },
  );
}

// StreamBuilder deleteOrder(orderRef, orderId, context) {
//   return StreamBuilder<QuerySnapshot>(
//       stream: orderRef.doc(orderId).delete().snapshot(),
//       builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
//           if(snapsh)
//       });
// }
