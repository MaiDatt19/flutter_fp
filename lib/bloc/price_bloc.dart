import 'dart:async';

import 'bloc.dart';

class SumCalculateBloc implements Bloc {
  final _priceCalculateController = StreamController<List<double>>();
  // 3
  Sink<List<double>> get priceCalculate => _priceCalculateController.sink;
  // 4
  late Stream<double> resultStream;

  SumCalculateBloc() {
    resultStream = _priceCalculateController.stream.asyncMap((values) {
      double sum = 0;
      for (var value in values) {
        sum += value;
        //
      }
      return sum;
    });
  }

  // 6
  @override
  void dispose() {
    _priceCalculateController.close();
  }
}
