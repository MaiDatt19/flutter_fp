import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/app.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../UI/sign_in.dart';
import '../UI/sign_up.dart';

Future<UserCredential> signInWithGoogle() async {
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication? googleAuth =
      await googleUser?.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth?.accessToken,
    idToken: googleAuth?.idToken,
  );

  // Once signed in, return the UserCredential
  UserCredential userCredential =
      await FirebaseAuth.instance.signInWithCredential(credential);
  if (userCredential.additionalUserInfo!.isNewUser) {
    print(userCredential.user.toString());
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection("users");
    users
        .doc(userCredential.user!.uid)
        .set(userCredential.additionalUserInfo!.profile)
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  return userCredential;
}

class StartUpScreen extends StatefulWidget {
  const StartUpScreen({Key? key}) : super(key: key);

  @override
  State<StartUpScreen> createState() => _StartUpScreenState();
}

class _StartUpScreenState extends State<StartUpScreen> {
  @override
  void initState() {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => AppHomePage()));
      } else {
        print('User is sign out in!');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        centerTitle: true,
        title: ImageIcon(
          const AssetImage('assets/logo.png'),
          size: 27,
          color: primaryColor,
        ),
      ),
      body: Container(
        color: Colors.white,
        padding:
            const EdgeInsets.only(top: 50, bottom: 20, left: 40, right: 40),
        child: Column(
          children: [
            const Spacer(),
            const Text(
              "Shop everything now!",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: ElevatedButton(
                onPressed: () => signInWithGoogle(),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Image(
                        image: AssetImage("assets/google_icon.png"),
                        width: 20,
                        height: 20,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                      ),
                      const Text(
                        "Continue with Google",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                ),
                style: ButtonStyle(
                    elevation: MaterialStateProperty.all<double?>(0),
                    backgroundColor:
                        MaterialStateProperty.all<Color?>(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: darkGrey, width: 0.5)))),
              ),
            ),
            const Divider(),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const SignUpScreen(),
                    ),
                  );
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Create account",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color?>(primaryColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      //side: BorderSide(color: greyTweet, width: 0.5)
                    ))),
              ),
            ),
            // Spacer(),
            FittedBox(
              fit: BoxFit.fitWidth,
              child: Row(
                children: [
                  Text("Have an account already?",
                      style: TextStyle(
                          fontSize: 15,
                          color: darkGrey,
                          fontWeight: FontWeight.w500)),
                  TextButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const SignInScreen(),
                        ),
                      );
                    },
                    child: Text("Login",
                        style: TextStyle(
                            fontSize: 15,
                            color: primaryColor,
                            fontWeight: FontWeight.w500)),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
