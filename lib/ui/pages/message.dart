import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:intl/intl.dart';

class MessageScreen extends StatefulWidget {
  // final String personOneId;
  final String receiverId;

  const MessageScreen({Key? key, required this.receiverId}) : super(key: key);

  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  String text = "";
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _controller.text = text;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection("conversations")
            .where('people', whereIn: [
          [widget.receiverId, getUid()],
          [getUid(), widget.receiverId]
        ])
            // .where('personTwoId', whereIn: [getUid(),widget.receiverId])
            .snapshots(),
        builder: (context, conversationSnapshot) {
          if (conversationSnapshot.hasData != true) {
            print("failed");
          }
          return Scaffold(
            // resizeToAvoidBottomInset: false,
            appBar: AppBar(
              foregroundColor: blackText,
              backgroundColor: tertiaryColor,
              elevation: 1,
              title: StreamBuilder<DocumentSnapshot>(
                stream: getDocumentByIdAsStream('shops', widget.receiverId),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(snapshot.data!['name']);
                  } else {
                    return Text("Customer");
                  }
                },
              ),
              actions: [
                IconButton(
                  icon: Icon(Icons.more_vert),
                  onPressed: () {},
                )
              ],
            ),

            body: SafeArea(
              child: Container(
                // height: MediaQuery.of(context).size.height,
                color: tertiaryColor,
                child: Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if (conversationSnapshot.hasData &&
                        conversationSnapshot.data!.docs.isNotEmpty)
                      StreamBuilder<QuerySnapshot>(
                          stream: FirebaseFirestore.instance
                              .collection("messages")
                              .where('conversationId',
                                  isEqualTo:
                                      conversationSnapshot.data!.docs.first.id)
                              .orderBy('sendTime', descending: false)
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Expanded(
                                // height: MediaQuery.of(context).size.height * 0.6,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) {
                                      print("senderId:" +
                                          snapshot.data!.docs[index]
                                              ['senderId']);
                                      if (snapshot.data!.docs[index]
                                              ['senderId'] ==
                                          getUid()) {
                                        //user sent message item
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(5.0),
                                              child: Text(
                                                DateFormat.MMMd()
                                                    .add_jm()
                                                    .format(snapshot.data!.docs
                                                        .first['sendTime']
                                                        .toDate())
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: darkGrey),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.2,
                                                  right: 10),
                                              decoration: BoxDecoration(
                                                  color: primaryColor,
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                          Radius.circular(20))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(15.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      snapshot.data!.docs[index]
                                                          ['text'],
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: tertiaryColor),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      } else if (snapshot.data!.docs[index]
                                              ['senderId'] !=
                                          getUid()) {
                                        print("receiver dc roi");
                                        //receiver sent message item
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5,
                                                      horizontal: 10),
                                              child: Text(
                                                DateFormat.MMMd()
                                                    .add_jm()
                                                    .format(snapshot.data!.docs
                                                        .first['sendTime']
                                                        .toDate())
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: darkGrey),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                  right: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.2,
                                                  left: 10),
                                              decoration: BoxDecoration(
                                                  color: lightGrey,
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                          Radius.circular(20))),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(15.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      snapshot.data!.docs[index]
                                                          ['text'],
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          color: blackText),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      } else {
                                        return Text("error");
                                      }
                                    }),
                              );
                            } else {
                              return Center(child: getCircularProgressBar());
                            }
                          }),
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.1,
                      margin: EdgeInsets.all(10),
                      alignment: Alignment.bottomLeft,
                      child: Flex(
                        direction: Axis.horizontal,
                        children: [
                          Expanded(
                            child: TextField(
                              controller: _controller,
                              onChanged: (String? value) {
                                text = value!;
                              },
                              cursorColor: primaryColor,
                              decoration: InputDecoration(
                                suffixIcon: IconButton(
                                    onPressed: () {
                                      FocusManager.instance.primaryFocus
                                          ?.unfocus();
                                      var now = DateTime(
                                        DateTime.now().year,
                                        DateTime.now().month,
                                        DateTime.now().day,
                                        DateTime.now().hour,
                                        DateTime.now().minute,
                                      );
                                      _controller.text = "";
                                      //send message
                                      if (conversationSnapshot.hasData &&
                                          conversationSnapshot
                                              .data!.docs.isEmpty &&
                                          text != "") {
                                        FirebaseFirestore.instance
                                            .collection("conversations")
                                            .add({
                                          'people': [
                                            getUid(),
                                            widget.receiverId
                                          ]
                                          // 'personOneId': getUid(),
                                          // 'personTwoId': widget.receiverId
                                        }).then((value) {
                                          //update last time conversation is active
                                          FirebaseFirestore.instance
                                              .collection("conversations")
                                              .doc(value.id)
                                              .update({
                                            'lastActiveTime': now,
                                          });

                                          FirebaseFirestore.instance
                                              .collection('messages')
                                              .add({
                                            'sendTime': now,
                                            'senderId': getUid(),
                                            'text': text,
                                            'conversationId': value.id
                                          }).then((value) {});
                                        });
                                      } else if (conversationSnapshot.hasData &&
                                          conversationSnapshot
                                              .data!.docs.isNotEmpty &&
                                          text != "") {
                                        FirebaseFirestore.instance
                                            .collection('messages')
                                            .add({
                                          'sendTime': now,
                                          'senderId': getUid(),
                                          'text': text,
                                          'conversationId': conversationSnapshot
                                              .data!.docs.first.id
                                        }).then((value) {
                                          FirebaseFirestore.instance
                                              .collection("conversations")
                                              .doc(conversationSnapshot
                                                  .data!.docs.first.id)
                                              .update({
                                            'lastActiveTime': now,
                                          });
                                        });
                                      }
                                    },
                                    icon: ImageIcon(
                                      const AssetImage("assets/message.png"),
                                      color: primaryColor,
                                    )),
                                contentPadding:
                                    const EdgeInsets.only(left: 10, right: 10),
                                fillColor: const Color(0xFFE7ECF0),
                                filled: true,
                                border: OutlineInputBorder(
                                    // gapPadding: 20,
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide: BorderSide.none),
                                hintText: 'Type something',
                                //suffixIcon: const Icon(Icons.check),
                                hintStyle:
                                    TextStyle(color: Colors.grey, fontSize: 15),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
