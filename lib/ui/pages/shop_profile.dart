import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/components/suggest_container.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:flutter_fp/ui/style/text_style.dart';

class ShopProfile extends StatefulWidget {
  const ShopProfile({Key? key, required this.id}) : super(key: key);
  //id of the user in firestore
  final String id;
  @override
  State<ShopProfile> createState() => _ShopProfileState();
}

class _ShopProfileState extends State<ShopProfile> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
        stream: getDocumentByIdAsStream('shops', widget.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
                appBar: AppBar(
                  backgroundColor: tertiaryColor,
                  foregroundColor: blackText,
                  title: Text(snapshot.data!['name']),
                ),
                body: Container(
                    color: tertiaryColor,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Container(
                          //   color: primaryColor,
                          //
                          //   height: MediaQuery.of(context).size.height * 0.3,
                          //   child: Column(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       Container(
                          //         margin: EdgeInsets.only(top: 20),
                          //       ),
                          //       BackButton(),
                          //       Spacer(),
                          //       Row(
                          //         // mainAxisAlignment: MainAxisAlignment.center,
                          //         crossAxisAlignment: CrossAxisAlignment.start,
                          //         children: [
                          //           //Avatar of the shop profile
                          //           Container(
                          //             margin: EdgeInsets.all(8),
                          //             child: CircleAvatar(
                          //               minRadius:
                          //                   MediaQuery.of(context).size.width *
                          //                       0.1,
                          //             ),
                          //           ),
                          //           //name of the shop
                          //         ],
                          //       ),
                          //     ],
                          //   ),
                          //   // decoration: BoxDecoration(),
                          // ),

                          if (snapshot.data!['description'] != "")
                            Column(
                              children: [
                                Text(
                                  "Introduction",
                                  style: getBigBlackText,
                                ),
                                Container(margin: EdgeInsets.only(top: 20)),
                                Text(snapshot.data!['description']),
                              ],
                            ),
                          const Divider(),
                          Row(
                            children: [
                              getCustomButton("Follow", () {}, Colors.white,
                                  Colors.purple, 0),
                              getCustomButton("Message", () {}, Colors.white,
                                  Colors.blue, 0),
                              // getCustomButton("Report", () {}, Colors.red, Colors.white)
                            ],
                          ),
                          //get products of the seller
                          getSuggestContaier("Collections", "seller", widget.id,
                              false, false, context)
                        ],
                      ),
                    )));
          } else {
            return getCircularProgressBar();
          }
        });
  }
}
