import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:image_picker/image_picker.dart';

class EditMyShop extends StatefulWidget {
  const EditMyShop({Key? key}) : super(key: key);

  @override
  State<EditMyShop> createState() => _EditMyShopState();
}

class _EditMyShopState extends State<EditMyShop> {
  final formKey = GlobalKey<FormState>();
  String? name = "";
  String? address = "";

  String? tel = "";
  // String? category = "Electronic";
  String? description = "";
  late File avatar;

  String? email = "";
  Map<String, Object?> data = {};
  // List<File> videoFiles = [];
  // String? dropdownValue = "Electronic";
  // late List<String> items = [];

  @override
  void initState() {
    // FirebaseFirestore.instance.collection("catergories").get().then((snapshot) {
    //   for (var snap in snapshot.docs) {
    //     print('catergories:' + snap.id);
    //     setState(() {
    //       items.add(snap.id.toString());
    //     });
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.only(
          top: 40,
          left: 20,
          right: 20,
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  const BackButton(),
                  const Flexible(
                    child: Text(
                      "My shop information",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const Spacer(),
                  getCustomButton("Update", () {
                    updateDocumentById('shops', getUid(), data);
                    Navigator.pop(context);
                  }, Colors.white, blackText, 1),
                ],
              ),
              StreamBuilder(
                  stream: getDocumentByIdAsStream('shops', getUid()),
                  builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          Form(
                              key: formKey,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(15),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, bottom: 5),
                                    child: const Text(
                                      "Shop name",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  TextFormField(
                                    // controller:
                                    //     TextEditingController(text: name),

                                    style: TextStyle(color: primaryColor),
                                    initialValue: snapshot.data!['name'],
                                    textAlignVertical: TextAlignVertical.center,
                                    textAlign: TextAlign.left,
                                    cursorColor: primaryColor,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      fillColor: const Color(0xFFE7ECF0),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          // gapPadding: 20,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide.none),
                                      hintText: 'e.g: Moon Shop',
                                      //suffixIcon: const Icon(Icons.check),
                                      hintStyle: TextStyle(
                                          color: Colors.grey, fontSize: 15),
                                    ),
                                    onChanged: (String? value) {
                                      name = value;
                                      // print(name);
                                      data['name'] = value;
                                    },
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, bottom: 5, top: 10),
                                    child: const Text(
                                      "Address",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  TextFormField(
                                    style: TextStyle(color: primaryColor),
                                    initialValue: snapshot.data!['address'],
                                    textAlignVertical: TextAlignVertical.center,
                                    textAlign: TextAlign.left,
                                    cursorColor: primaryColor,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      fillColor: const Color(0xFFE7ECF0),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          // gapPadding: 20,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide.none),
                                      hintText: 'e.g: District 7, HCMC',
                                      //suffixIcon: const Icon(Icons.check),
                                      hintStyle: TextStyle(
                                          color: Colors.grey, fontSize: 15),
                                    ),
                                    onChanged: (String? value) {
                                      address = value;
                                      // print('address:' + address!);
                                      data['address'] = value;
                                    },
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, bottom: 5, top: 10),
                                    child: const Text(
                                      "Email",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  TextFormField(
                                    style: TextStyle(color: primaryColor),
                                    initialValue: snapshot.data!['email'],
                                    textAlignVertical: TextAlignVertical.center,
                                    textAlign: TextAlign.left,
                                    cursorColor: primaryColor,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      fillColor: const Color(0xFFE7ECF0),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          // gapPadding: 20,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide.none),
                                      hintText: 'e.g: shop@example.com',
                                      //suffixIcon: const Icon(Icons.check),
                                      hintStyle: TextStyle(
                                          color: Colors.grey, fontSize: 15),
                                    ),
                                    onChanged: (String? value) {
                                      email = value;
                                      // print('email:' + email!);
                                      data['email'] = value;
                                    },
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, bottom: 5, top: 10),
                                    child: const Text(
                                      "Phone Number",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  TextFormField(
                                    keyboardType: TextInputType.number,
                                    initialValue: snapshot.data!['tel'],
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r'[0-9]')),
                                    ],
                                    style: TextStyle(color: primaryColor),
                                    textAlignVertical: TextAlignVertical.center,
                                    textAlign: TextAlign.left,
                                    cursorColor: primaryColor,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      fillColor: const Color(0xFFE7ECF0),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          // gapPadding: 20,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide.none),
                                      hintText: 'e.g: 08823...',
                                      //suffixIcon: const Icon(Icons.check),
                                      hintStyle: TextStyle(
                                          color: Colors.grey, fontSize: 15),
                                    ),
                                    onChanged: (String? value) {
                                      tel = value;
                                      // print('email:' + email!);
                                      // print('tel:' + tel!);
                                      data['tel'] = value;
                                    },
                                  ),
                                  Container(
                                      margin: const EdgeInsets.only(
                                          left: 10, bottom: 5, top: 10),
                                      child: const Text(
                                        "Description",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  TextFormField(
                                    style: TextStyle(color: primaryColor),
                                    initialValue: snapshot.data!['description'],
                                    textAlignVertical: TextAlignVertical.center,
                                    textAlign: TextAlign.left,
                                    cursorColor: primaryColor,
                                    decoration: InputDecoration(
                                      contentPadding: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      fillColor: const Color(0xFFE7ECF0),
                                      filled: true,
                                      border: OutlineInputBorder(
                                          // gapPadding: 20,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          borderSide: BorderSide.none),
                                      hintText: 'Enter description',
                                      //suffixIcon: const Icon(Icons.check),
                                      hintStyle: TextStyle(
                                          color: Colors.grey, fontSize: 15),
                                    ),
                                    onChanged: (String? value) {
                                      description = value;
                                      // print('description:' + description!);
                                      data['description'] = value;
                                    },
                                  ),
                                  getCustomButton("Upload avatar", () async {
                                    XFile? pickedFile = await ImagePicker()
                                        .pickImage(source: ImageSource.gallery);

                                    if (pickedFile != null) {
                                      setState(() {
                                        avatar = (File(pickedFile.path));
                                      });
                                    }
                                  }, Colors.white, blackText, 1),
                                  getCustomButton("Take new Avatar", () async {
                                    XFile? pickedFile = await ImagePicker()
                                        .pickImage(source: ImageSource.camera);

                                    if (pickedFile != null) {
                                      setState(() {
                                        avatar = (File(pickedFile.path));
                                      });
                                    }
                                  }, Colors.white, blackText, 1)
                                ],
                              )),
                        ],
                      );
                    } else {
                      return Text("");
                    }
                  }),
              // uploadedVideoList(),
              // uploadedImagesList(),
            ],
          ),
        ),
      ),
    );
  }

  // Widget uploadedImagesList() {
  //   return ListView.builder(
  //       scrollDirection: Axis.vertical,
  //       physics: const NeverScrollableScrollPhysics(),
  //       shrinkWrap: true,
  //       itemCount: imageFiles.length,
  //       itemBuilder: (BuildContext context, int index) {
  //         if (imageFiles.isEmpty) {
  //           return const Text("No image");
  //         } else {
  //           // return Text(imageFiles[index].path);
  //           print(imageFiles[index].statSync());
  //           return Container(
  //             margin: const EdgeInsets.symmetric(vertical: 20),
  //             child: Image.file(
  //               imageFiles[index],
  //             ),
  //           );
  //         }
  //       });
  // }

//   Widget CreateProduct() {
//     return ElevatedButton(
//         style: ButtonStyle(
//             backgroundColor: MaterialStateProperty.all<Color?>(Colors.white),
//             shape: MaterialStateProperty.all<RoundedRectangleBorder>(
//                 RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(30.0),
//               //side: BorderSide(color: greyTweet, width: 0.5)
//             ))),
//         onPressed: () async {
//           final storage = FirebaseStorage.instance;
//           final storageRef = FirebaseStorage.instance.ref();
//           final imagesRef = storageRef.child("images");
//           // final spaceRef = storageRef.child("images/space.jpg");
//           final metadata = SettableMetadata(contentType: "image/jpeg");
//           // final appDocDir = await getApplicationDocumentsDirectory();
//           // final filePath = "${appDocDir.absolute}/path/to/mountains.jpg";
//           // final file = File(filePath);
//           //   });
//           // final uploadTask = storageRef
//           //     .child("images/path/to/mountains.jpg")
//           //     .putFile(file, metadata);
//           int i = 0;
//           // for (var file in imageFiles) {
//           //   imagesRef
//           //       .child(name! +
//           //           "_" +
//           //           description! +
//           //           "_" +
//           //           price!.toString() +
//           //           "_" +
//           //           i.toString())
//           //       .putFile(file);
//           //   i += 1;
//           // }
// // Listen for state changes, errors, and completion of the upload.
//           // uploadTask.snapshotEvents.listen((TaskSnapshot taskSnapshot) {
//           //   switch (taskSnapshot.state) {
//           //     case TaskState.running:
//           //       final progress = 100.0 *
//           //           (taskSnapshot.bytesTransferred / taskSnapshot.totalBytes);
//           //       print("Upload is $progress% complete.");
//           //       break;
//           //     case TaskState.paused:
//           //       print("Upload is paused.");
//           //       break;
//           //     case TaskState.canceled:
//           //       print("Upload was canceled");
//           //       break;
//           //     case TaskState.error:
//           //       // Handle unsuccessful uploads
//           //       break;
//           //     case TaskState.success:
//           //       // Handle successful uploads on complete
//           //       // ...
//           //       break;
//           //   }
//           // });
//           // widget.productsRef.add({
//           //   'name': name, // John Doe
//           //   'price': price, // Stokes and Sons
//           //   'category': category,
//           //   'description': description, // 42
//           //   'seller': FirebaseAuth.instance.currentUser!.uid.toString(),
//           // }).then((value) {
//           //   print(value.id);
//           //   int i = 0;
//           //   for (var img in imageFiles) {
//           //     imagesRef.child(value.id).child(i.toString()).putFile(img);
//           //     i += 1;
//           //   }
//           //   for (var video in videoFiles) {
//           //     imagesRef.child(value.id).child(i.toString()).putFile(video);
//           //     i += 1;
//           //   }
//           // });
//           //
//           // widget.changeIndexedStack(0);
//
//           // print("name=$name");
//           // print("price=$price");
//           // print("category=$category");
//           //   print('email=$eail');
//           //   print('Demo only: password=$password');
//           // }
//         },
//         child: const Text(
//           'Create',
//           style: TextStyle(color: Colors.black),
//         ));
//   }
}
