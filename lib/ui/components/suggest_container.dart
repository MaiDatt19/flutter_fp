import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/components/product_card.dart';
import 'package:page_transition/page_transition.dart';

import '../main_pages/homepageScreen/pageofNewarrivals/NewArrivalsPage.dart';
import '../style/colors.dart';
import 'label.dart';

final storageRef = FirebaseStorage.instance.ref();
final imagesRef = storageRef.child("images");
final productsRef = FirebaseFirestore.instance.collection("products");

// Widget getSuggestSlider( tittle, isHorizontal, special, context){
//         return Container(
//         child: Column(
//       children: [
//         InkWell(
//           child: IntrinsicWidth(
//             child: Container(
//               width: 500,
//               color: Colors.white,
//               child: ListTile(
//                 title: Text('Just For You',
//                     style: TextStyle(
//                         color: Colors.black,
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold)),
//                 trailing: Icon(
//                   Icons.arrow_forward,
//                 ),
//               ),
//             ),
//           ),
//           onTap: () {
//             Navigator.push(
//                 context,
//                 PageTransition(
//                     child: NewArrivalsPage(),
//                     type: PageTransitionType.rightToLeft));
//           },
//         ),
//         Container(
//           child: CarouselSlider(
//             options: CarouselOptions(
//               enlargeCenterPage: true,
//               enableInfiniteScroll: false,
//               autoPlay: true,
//             ),
//             items: imageList
//                 .map((e) => ClipRect(
//                       child: Stack(
//                         fit: StackFit.expand,
//                         children: <Widget>[
//                           Image.network(e,
//                               width: 100, height: 200, fit: BoxFit.cover)
//                         ],
//                       ),
//                     ))
//                 .toList(),
//           ),
//         ),
//       ],
//     ));
// }

Widget getSuggestContaier(
    containerTitle, fieldName, filter, isHorizontal, special, context) {
  if (isHorizontal) {
    return SizedBox(
        width: 100,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                child: IntrinsicWidth(
                  child: Container(
                    width: 500,
                    color: Colors.white,
                    child: ListTile(
                      title: Text(containerTitle,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                      trailing: Icon(
                        Icons.arrow_back_ios,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const NewArrivalsPage(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
              SizedBox(
                height: 300,
                child: StreamBuilder<QuerySnapshot>(
                    stream: fieldName == ""
                        ? productsRef.snapshots()
                        : productsRef
                            .where(fieldName, isEqualTo: filter)
                            .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return const Text('Something went wrong');
                      }

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Text("Loading");
                      }
                      return ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                              Map<String, dynamic> data =
                                  document.data()! as Map<String, dynamic>;
                              var imageRef =
                                  imagesRef.child(document.id).child("0");
                              // print("id:" + document.id);

                              return FutureBuilder(
                                future: imageRef.getDownloadURL(),
                                builder: (BuildContext context, url) {
                                  if (url.hasData) {
                                    // print("link:" + url.data.toString());
                                    return getProductCardNoPrice(
                                        document.id,
                                        data,
                                        CachedNetworkImageProvider(
                                            url.data.toString()),
                                        context);
                                  } else {
                                    return getProductCardNoPrice(
                                        document.id,
                                        data,
                                        const AssetImage("assets/loading.gif"),
                                        context);
                                  }
                                },
                              );
                            })
                            .toList()
                            .cast(),
                      );
                    }),
              ),
            ]));
  } else {
    //grid
    return Column(
      children: [
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              width: 500,
              color: Colors.white,
              child: ListTile(
                title: Text(containerTitle,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                trailing: Icon(
                  Icons.arrow_back_ios,
                ),
              ),
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    child: const NewArrivalsPage(),
                    type: PageTransitionType.rightToLeft));
          },
        ),
        StreamBuilder<QuerySnapshot>(
            stream: fieldName == ""
                ? productsRef.snapshots()
                : productsRef.where(fieldName, isEqualTo: filter).snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return const Text('Something went wrong');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Text("Loading");
              } else {
                return GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 0.6,
                  mainAxisSpacing: 10,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: snapshot.data!.docs
                      .map((DocumentSnapshot document) {
                        Map<String, dynamic> data =
                            document.data()! as Map<String, dynamic>;
                        var imageRef = imagesRef.child(document.id).child("0");
                        // print("id:" + document.id);

                        return FutureBuilder(
                          future: imageRef.getDownloadURL(),
                          builder: (BuildContext context, url) {
                            if (url.hasData) {
                              // print("link:" + url.data.toString());
                              return getProductCard(
                                  CachedNetworkImageProvider(
                                      url.data.toString()),
                                  data['name'],
                                  getLabel('Online customization', lightGrey,
                                      darkGrey),
                                  data['price'],
                                  document.id,
                                  context);
                            } else {
                              return getProductCard(
                                  const AssetImage("assets/loading.gif"),
                                  data['name'],
                                  getLabel('Online customization', lightGrey,
                                      darkGrey),
                                  data['price'],
                                  document.id,
                                  context);
                            }
                          },
                        );
                      })
                      .toList()
                      .cast(),
                  // [
                  //   getProductCard(
                  //       NetworkImage(
                  //           'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWk5J3VAkTEipsVQdf-K06l2CUsYKBF283eg&usqp=CAU'),
                  //       'AAAA',
                  //       getLabel('Online customization', lightGrey, darkGrey),
                  //       '124',
                  //       'asdsadsada'),
                  //   getProductCard(
                  //       NetworkImage(
                  //           'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWk5J3VAkTEipsVQdf-K06l2CUsYKBF283eg&usqp=CAU'),
                  //       'AAAA',
                  //       getLabel('Online customization', lightGrey, darkGrey),
                  //       '124',
                  //       'asdsadsada'),
                  //   getProductCard(
                  //       NetworkImage(
                  //           'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWk5J3VAkTEipsVQdf-K06l2CUsYKBF283eg&usqp=CAU'),
                  //       'AAAA',
                  //       getLabel('Online customization', lightGrey, darkGrey),
                  //       '124',
                  //       'asdsadsada'),
                  // ],
                  // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //     crossAxisCount: 2,
                  //     mainAxisSpacing: 10,
                  //     crossAxisSpacing: 10,
                  //     childAspectRatio: 0.55),
                );
              }
            }),
      ],
    );
  }
}
