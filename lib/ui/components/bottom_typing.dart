import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/style/colors.dart';

class BottomTypingBar extends StatefulWidget {
  const BottomTypingBar({Key? key}) : super(key: key);

  @override
  State<BottomTypingBar> createState() => _BottomTypingBarState();
}

class _BottomTypingBarState extends State<BottomTypingBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Divider(
          height: 2,
          thickness: 1,
        ),
        Row(
          children: [
            const Expanded(
              child: TextField(
                decoration: InputDecoration(hintText: "Tweet your reply"),
              ),
            ),
            IconButton(
                onPressed: () => {},
                icon: ImageIcon(
                  AssetImage("assets/send.png"),
                  color: primaryColor,
                ))
          ],
        ),
      ],
    );
  }
}
