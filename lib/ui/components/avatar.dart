import 'package:flutter/material.dart';

class AvatarBox extends StatefulWidget {
  const AvatarBox({Key? key, String? url}) : super(key: key);
  @override
  State<AvatarBox> createState() => _AvatarBoxState();
}

class _AvatarBoxState extends State<AvatarBox> {
  late String url;
  late CircleAvatar circleAvatar;
  @override
  void initState() {
    super.initState();
    circleAvatar = CircleAvatar(
      radius: 23.0,
      backgroundImage: const AssetImage("assets/user.png"),
      backgroundColor: Colors.grey[200],
    );
  }

  @override
  Widget build(BuildContext context) {
    return circleAvatar;
  }
}
