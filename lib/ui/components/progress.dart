import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/style/colors.dart';

Widget getCircularProgressBar() {
  return CircularProgressIndicator(
    color: primaryColor,
  );
}
