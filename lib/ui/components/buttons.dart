import 'package:flutter/material.dart';

ElevatedButton getCustomButton(
    text, ontap, backgroundColor, textColor, double elevation) {
  return ElevatedButton(
    onPressed: ontap,
    child: Text(
      text,
      style: TextStyle(color: textColor),
    ),
    style: ButtonStyle(
        elevation: MaterialStateProperty.all<double?>(elevation),
        backgroundColor: MaterialStateProperty.all<Color?>(backgroundColor),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
          //side: BorderSide(color: greyTweet, width: 0.5)
        ))),
  );
}

// DropdownButton getDropdownButton(items, Function onSave) {
//   String? dropdownValue = items[0];
//   return
// }
