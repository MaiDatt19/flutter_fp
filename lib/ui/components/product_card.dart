import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/main_pages/product_detail.dart';
import 'package:page_transition/page_transition.dart';

Widget getProductCard(img, tittle, label, price, id, context) {
  return InkWell(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ProductDetailScreen(id: id)),
      );
    },
    child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 255, 255, 255),
            borderRadius: BorderRadius.circular(10)),
        child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(image: img, fit: BoxFit.fill)),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
              ),
              Text(tittle,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.normal)),
              Container(
                margin: const EdgeInsets.only(top: 20),
              ),
              Text(
                '\$' + price.toString(),
                style:
                    const TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
              ),
            ])),
  );
}

Widget getProductCardNoPrice(id, data, image, context) {
  return InkWell(
    onTap: () {
      Navigator.push(
          context,
          PageTransition(
              child: ProductDetailScreen(id: id),
              alignment: Alignment.center,
              type: PageTransitionType.bottomToTop));
    },
    child: Container(
      padding: const EdgeInsets.all(10),
      width: 200.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                  image: image,
                  // image: NetworkImage(imgUrl),
                  fit: BoxFit.fill),
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, left: 5),
            child: Text(
              data['name'],
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    ),
  );
}
