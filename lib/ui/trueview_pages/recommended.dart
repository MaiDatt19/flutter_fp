import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/trueview_pages/hosttopicsmore.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:page_transition/page_transition.dart';
import 'package:transparent_image/transparent_image.dart';

class RecommendedPage extends StatefulWidget {
  const RecommendedPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return RecommendedPageState();
  }
}

class CardItem {
  final String urlImage;
  final String tittle;

  const CardItem({
    required this.urlImage,
    required this.tittle,
  });
}

List<String> imageList = [
  'https://cdn.pixabay.com/photo/2019/03/15/09/49/girl-4056684_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/12/15/16/25/clock-5834193__340.jpg',
  'https://cdn.pixabay.com/photo/2020/09/18/19/31/laptop-5582775_960_720.jpg',
  'https://media.istockphoto.com/photos/woman-kayaking-in-fjord-in-norway-picture-id1059380230?b=1&k=6&m=1059380230&s=170667a&w=0&h=kA_A_XrhZJjw2bo5jIJ7089-VktFK0h0I4OWDqaac0c=',
  'https://cdn.pixabay.com/photo/2019/11/05/00/53/cellular-4602489_960_720.jpg',
  'https://cdn.pixabay.com/photo/2017/02/12/10/29/christmas-2059698_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/01/29/17/09/snowboard-4803050_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/02/06/20/01/university-library-4825366_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/11/22/17/28/cat-5767334_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/12/13/16/22/snow-5828736_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/12/09/09/27/women-5816861_960_720.jpg',
  'https://cdn.pixabay.com/photo/2019/03/15/09/49/girl-4056684_960_720.jpg',
  'https://cdn.pixabay.com/photo/2020/12/15/16/25/clock-5834193__340.jpg',
];

class RecommendedPageState extends State<RecommendedPage> {
  List<CardItem> items = [
    const CardItem(
      urlImage:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPETDEkeU2NMUywRMWSophyIrs4n9_45xbhg&usqp=CAU",
      tittle: "Xin chao anh",
    ),
    const CardItem(
      urlImage:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4WgBwvO2wf0pyul0pczQ-WEq0r0kwPflI2Q&usqp=CAU",
      tittle: "tam biet",
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: Column(
            children: [
              Container(
                height: 50,
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Expanded(
                        child: Row(
                      children: [
                        const Text(
                          'Hot Topics',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 17,
                              fontFamily: '.SF UI Display'),
                        ),
                        const Spacer(),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: const HostTopicMore(),
                                      type: PageTransitionType.rightToLeft));
                            },
                            icon: const Icon(
                              Icons.arrow_forward,
                              color: Colors.black,
                            ))
                      ],
                    ))
                  ],
                ),
              ),
              // SizedBox(
              //   height: 200,
              //   child: ListView(
              //     scrollDirection: Axis.horizontal,
              //     children: <Widget>[
              //       Padding(
              //         padding: const EdgeInsets.all(8.0),
              //         child: InkWell(
              //           onTap: () {},
              //           child: Container(
              //             alignment: Alignment.bottomCenter,
              //             child: const Text("alibaba",
              //                 style: TextStyle(
              //                     color: Colors.white,
              //                     fontWeight: FontWeight.bold)),
              //             height: 150,
              //             width: 150,
              //             decoration: BoxDecoration(
              //               borderRadius: BorderRadius.circular(15),
              //               image: const DecorationImage(
              //                 image: NetworkImage(
              //                     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRscDAbLzrIJHJkBsA7OLi_hDzKsQFRxUPiaw&usqp=CAU"),
              //                 fit: BoxFit.fill,
              //               ),
              //             ),
              //           ),
              //         ),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.all(8.0),
              //         child: InkWell(
              //           onTap: () {},
              //           child: Container(
              //             alignment: Alignment.bottomCenter,
              //             child: const Text("alibaba",
              //                 style: TextStyle(
              //                     color: Colors.white,
              //                     fontWeight: FontWeight.bold)),
              //             height: 150,
              //             width: 150,
              //             decoration: BoxDecoration(
              //               borderRadius: BorderRadius.circular(15),
              //               image: const DecorationImage(
              //                 image: NetworkImage(
              //                     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRscDAbLzrIJHJkBsA7OLi_hDzKsQFRxUPiaw&usqp=CAU"),
              //                 fit: BoxFit.fill,
              //               ),
              //             ),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Just For You",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 900,
                  child: StaggeredGridView.countBuilder(
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 12,
                      itemCount: 12,
                      itemBuilder: (context, index) {
                        return Container(
                          decoration: const BoxDecoration(
                              color: Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(15)),
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: imageList[index],
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                      staggeredTileBuilder: (index) {
                        return StaggeredTile.count(1, index.isEven ? 1.2 : 1.8);
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
