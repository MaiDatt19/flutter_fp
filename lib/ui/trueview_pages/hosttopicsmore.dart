import 'package:flutter/material.dart';

class HostTopicMore extends StatefulWidget {
  const HostTopicMore({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HostTopicMoreState();
  }
}

class HostTopicMoreState extends State<HostTopicMore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 30,
        backgroundColor: Colors.white,
        title: const Text(
          'Hot Topics',
          style: TextStyle(color: Colors.black, fontSize: 17),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            InkWell(
                child: Container(
              width: MediaQuery.of(context).size.width,
              height: 200,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWZhmMyaODLHhpW-wGGpyRgwl3xFk8pc_sIw&usqp=CAU'),
                      fit: BoxFit.cover),
                  color: Colors.yellow),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 8),
                    alignment: Alignment.centerLeft,
                    child: TextButton(
                      child: const Text(
                        'Edittor choice',
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              const Color.fromARGB(255, 0, 0, 0))),
                      onPressed: () {},
                    ),
                  ),
                  const Spacer(),
                  InkWell(
                    child: IntrinsicWidth(
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          backgroundBlendMode: BlendMode.color,
                          color: const Color.fromARGB(255, 202, 202, 202)
                              .withOpacity(0.9),
                        ),
                        width: 500,
                        child: ListTile(
                          title: const Text('# Summer hot topics',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                          subtitle: const Padding(
                            padding: EdgeInsets.only(bottom: 10.0),
                            child: Text(
                              '8.6k videos',
                            ),
                          ),
                          trailing: IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.arrow_forward,
                            ),
                          ),
                        ),
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            )),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const SizedBox(
              height: 10,
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.red,
                  child: ListTile(
                    title: const Text('# Daily travel not-to-miss',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    subtitle: const Text('501'),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        'https://cdn.pixabay.com/photo/2016/01/20/13/05/cat-1151519__340.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
          ]))
        ],
      ),
    );
  }
}
