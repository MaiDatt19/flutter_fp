import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/style/colors.dart';

class FollowingPage extends StatefulWidget {
  const FollowingPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FollowingPageState();
  }
}

class FollowingPageState extends State<FollowingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: tertiaryColor,
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const CircleAvatar(
                              backgroundImage: AssetImage("assets/user.png")),
                          //company name//
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    'Moon Shop',
                                    // overflow: TextOverflow.clip,
                                    // softWrap: false,
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      fontFamily: 'Roboto',
                                      color: blackText,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    'Wednesday',
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                          ),
                          // const Spacer(),
                          const Icon(Icons.more_horiz),
                        ]),
                    //image product
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Text(
                        'New model S22 is coming with good price',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Roboto',
                          color: Color(0xFF212121),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fitWidth,
                          image:
                              NetworkImage("https://picsum.photos/250?image=9"),
                        ),
                      ),
                    ),
                    // FittedBox(
                    //   fit: BoxFit.fill,
                    //   child: Image(
                    //     width: MediaQuery.of(context).size.width,
                    //     image: NetworkImage(
                    //         "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeuhqlskXcvmmuj1pVUCLLr3aUxJCs1zLc4g&usqp=CAU"),
                    //   ),
                    // ),
                    // container/listview image
                    SizedBox(
                      height: 80,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          //cotainer/product lsiview
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 180,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: secondaryColor,
                              ),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
                                      height: 60,
                                      width: 50,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          image: const DecorationImage(
                                              image: NetworkImage(
                                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnVCx7whW9-ygvGulnZwBOMHIzbvoMqVFW7Q&usqp=CAU'))),
                                    ),
                                  ),
                                  //container/price
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: const [
                                      Padding(
                                        padding: EdgeInsets.only(right: 45),
                                        child: Text(
                                          '%38.88',
                                        ),
                                      ),
                                      Text('1 Piece(MOQ)')
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          ),
                          //cotainer/product lsiview
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 180,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: secondaryColor,
                              ),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
                                      height: 60,
                                      width: 50,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          image: const DecorationImage(
                                              image: NetworkImage(
                                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnVCx7whW9-ygvGulnZwBOMHIzbvoMqVFW7Q&usqp=CAU'))),
                                    ),
                                  ),
                                  //container/price
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: const [
                                      Padding(
                                        padding: EdgeInsets.only(right: 45),
                                        child: Text(
                                          '%38.88',
                                        ),
                                      ),
                                      Text('1 Piece(MOQ)')
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          ),
                          //cotainer/product lsiview
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 180,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: secondaryColor,
                              ),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
                                      height: 60,
                                      width: 50,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          image: const DecorationImage(
                                              image: NetworkImage(
                                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnVCx7whW9-ygvGulnZwBOMHIzbvoMqVFW7Q&usqp=CAU'))),
                                    ),
                                  ),
                                  //container/price
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: const [
                                      Padding(
                                        padding: EdgeInsets.only(right: 45),
                                        child: Text(
                                          '%38.88',
                                        ),
                                      ),
                                      Text('1 Piece(MOQ)')
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {},
                            icon:
                                ImageIcon(AssetImage("assets/heart_solid.png")),
                            color: primaryColor,
                          ),
                          Text('8'),
                          Spacer(),
                          Icon(
                            Icons.chat_bubble_outline,
                            color: primaryColor,
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text('Chat'),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ))
        ],
      ),
    ));
  }
}
