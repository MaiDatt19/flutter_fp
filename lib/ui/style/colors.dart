import 'package:flutter/material.dart';

Color blackText = const Color(0xFF1D1D1D);
Color darkGrey = const Color(0xFF8F8F8F);
Color lightGrey = const Color(0xFFF6F6F6);
Color lightBlack = const Color(0xFF656364);
Color mediumGrey = const Color.fromARGB(255, 190, 190, 198);

Color? primaryColor = const Color.fromARGB(255, 94, 101, 204);

Color tertiaryColor = Colors.white;

Color secondaryColor = const Color.fromARGB(255, 229, 229, 255);

// Color primaryColor = const Color.fromARGB(255, 234, 125, 30);
