import 'package:flutter/material.dart';

TextStyle getSmallProfileNameStyle() {
  return const TextStyle(
      color: Colors.black,
      fontFamily: ".SF Pro Text",
      fontSize: 17,
      fontWeight: FontWeight.bold);
}

// TextStyle getTweetTextStyle() {
//   return const TextStyle(
//       color: Colors.black, fontFamily: ".SF Pro Text", fontSize: 20);
// }

TextStyle greyText = const TextStyle(
    color: Colors.grey,
    fontFamily: ".SF Pro Text",
    fontSize: 17,
    decorationThickness: 2);

TextStyle getBigWhiteText = const TextStyle(
    color: Colors.white,
    fontFamily: ".SF Pro Text",
    fontSize: 24,
    decorationThickness: 2);
TextStyle superBigBlackText = const TextStyle(
    color: Colors.black,
    fontFamily: ".SF Pro Text",
    fontSize: 30,
    decorationThickness: 2);

TextStyle getSmallWhiteText = const TextStyle(
    color: Colors.white,
    fontFamily: ".SF Pro Text",
    fontSize: 18,
    decorationThickness: 2);

TextStyle getTopicWhiteText = const TextStyle(
    color: Colors.white,
    fontFamily: ".SF Pro Text",
    fontSize: 18,
    fontWeight: FontWeight.bold);

TextStyle getBigBlackText = const TextStyle(
    color: Colors.black,
    fontFamily: ".SF Pro Text",
    fontSize: 20,
    decorationThickness: 2);
