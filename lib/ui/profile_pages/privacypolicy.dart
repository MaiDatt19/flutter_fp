import 'package:flutter/material.dart';

class PrivacyPolicyScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PrivacyPolicyScreenState();
  }
}

class PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Privacy Policy'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: Text('PrivacyPolicy'),
    );
  }
}
