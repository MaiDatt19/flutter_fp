import 'package:flutter/material.dart';

class ShipDestinationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ShipDestinationScreenState();
  }
}

class ShipDestinationScreenState extends State<ShipDestinationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ship to'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: Text('ShipDestination'),
    );
  }
}
