import 'package:flutter/material.dart';

class LicenseUseScreen extends StatefulWidget {
  const LicenseUseScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LicenseUseScreenState();
  }
}

class LicenseUseScreenState extends State<LicenseUseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('License Use'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: const Text('LicenseUse'),
    );
  }
}
