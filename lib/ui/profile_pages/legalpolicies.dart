import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/profile_pages/authorizationmanagement.dart';
import 'package:flutter_fp/ui/profile_pages/cookiepreferences.dart';
import 'package:flutter_fp/ui/profile_pages/license_use.dart';
import 'package:flutter_fp/ui/profile_pages/privacypolicy.dart';
import 'package:page_transition/page_transition.dart';

class LegalPoliciesScreen extends StatefulWidget {
  const LegalPoliciesScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LegalPoliciesScreenState();
  }
}

class LegalPoliciesScreenState extends State<LegalPoliciesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Legal Policies'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(
              delegate: SliverChildListDelegate([
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: const ListTile(
                    title: Text('Terms of License and Use',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: const LicenseUseScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: const ListTile(
                    title: Text('Privacy Policy',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: PrivacyPolicyScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: const ListTile(
                    title: Text('Cookie Preferences',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: CookiePreferencesScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: const ListTile(
                    title: Text('Authorization Management',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: AuthorizaitonManagementScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
          ]))
        ],
      ),
    );
  }
}
