import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/profile_pages/currency.dart';
import 'package:flutter_fp/ui/profile_pages/legalpolicies.dart';
import 'package:flutter_fp/ui/profile_pages/shipdestination.dart';
import 'package:page_transition/page_transition.dart';

import '../../UI/app.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SettingsScreenState();
  }
}

class SettingsScreenState extends State<SettingsScreen> {
  // var ship_destination =

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            InkWell(
              child: IntrinsicWidth(
                  child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Ship to',
                      style: TextStyle(color: Colors.black, fontSize: 18)),
                  trailing: Text(
                    'Đà Nẵng',
                    style: TextStyle(color: Colors.orange),
                  ),
                ),
              )),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: ShipDestinationScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text('Currency',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    trailing: Text(
                      'USD',
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: CurrencySettingScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text('Language',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                    trailing: Text(
                      'English',
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                ),
              ),
              onTap: () {},
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text('Notifications',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {},
            ),
            const Divider(
              thickness: 1,
            ),
            InkWell(
              child: IntrinsicWidth(
                  child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Version',
                      style: TextStyle(color: Colors.black, fontSize: 18)),
                  subtitle: Text('0.1.1'),
                  trailing: Text(
                    'UPDATE',
                    style: TextStyle(color: Colors.orange),
                  ),
                ),
              )),
              onTap: () {},
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text('Legal Policies',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: LegalPoliciesScreen(),
                        type: PageTransitionType.rightToLeft));
              },
            ),
            InkWell(
              child: IntrinsicWidth(
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    title: Text('Rate the App',
                        style: TextStyle(color: Colors.black, fontSize: 18)),
                  ),
                ),
              ),
              onTap: () {},
            ),
            if (FirebaseAuth.instance.currentUser != null) Spacer(),
            if (FirebaseAuth.instance.currentUser != null)
              getCustomButton("Sign Out", () {
                FirebaseAuth.instance.signOut().then((value) {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => AppHomePage()));
                });
              }, Colors.black, Colors.white, 1)
          ])),
        ],
      ),
    );
  }
}
