import 'package:flutter/material.dart';

class CurrencySettingScreen extends StatefulWidget {
  const CurrencySettingScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CurrencySettingScreenState();
  }
}

class CurrencySettingScreenState extends State<CurrencySettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select Currency'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: const Text('CurrencySetting'),
    );
  }
}
