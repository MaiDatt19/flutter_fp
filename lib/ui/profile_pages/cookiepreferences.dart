import 'package:flutter/material.dart';

class CookiePreferencesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CookiePreferencesScreenState();
  }
}

class CookiePreferencesScreenState extends State<CookiePreferencesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cookie Preferences'),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: const Text('CookiePreferences'),
    );
  }
}
