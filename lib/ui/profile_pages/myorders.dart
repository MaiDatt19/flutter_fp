import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/style/colors.dart';

import '../manage_order_menu/all_page.dart';

class MyOrdersScreen extends StatefulWidget {
  const MyOrdersScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyOrdersScreenState();
  }
}

class MyOrdersScreenState extends State<MyOrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            centerTitle: true,
            title: Text(
              'Manage Order',
              style: TextStyle(fontSize: 16.0),
            ),
            bottom: PreferredSize(
                child: Container(
                    color: Colors.white,
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: TabBar(
                              labelPadding:
                                  EdgeInsets.symmetric(horizontal: 10.0),
                              isScrollable: true,
                              labelColor: primaryColor,
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 17),
                              unselectedLabelStyle: TextStyle(),
                              unselectedLabelColor: darkGrey,
                              indicator: UnderlineTabIndicator(
                                borderSide: BorderSide(
                                  color: primaryColor!,
                                  width: 3.0,
                                ),
                              ),
                              tabs: [
                                Tab(
                                  child: Text('All'),
                                ),
                                Tab(
                                  child: Text('To be confirmed'),
                                ),
                                Tab(
                                  child: Text('Unpaid'),
                                ),
                                Tab(
                                  child: Text(' Processing'),
                                ),
                              ]),
                        ),
                      ],
                    )),
                preferredSize: Size.fromHeight(30.0)),
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: IconButton(
                      onPressed: () {}, icon: Icon(Icons.headphones_rounded))),
            ],
          ),
          body: TabBarView(
            children: [
              AllPage(),
              AllPage(),
              Container(
                child: Center(
                  child: Text('Tab 3'),
                ),
              ),
              Container(
                child: Center(
                  child: Text('Tab 4'),
                ),
              ),
            ],
          )),
    );
  }
}
