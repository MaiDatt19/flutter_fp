import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/trueview_pages/following.dart';
import 'package:flutter_fp/ui/trueview_pages/recommended.dart';
import 'package:fluttericon/typicons_icons.dart';

class TrueViewScreen extends StatefulWidget {
  const TrueViewScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TrueViewScreenState();
  }
}

class TrueViewScreenState extends State<TrueViewScreen> {
  int currentScreen = 0;
  final screen = [
    FollowingPage(),
    RecommendedPage(),
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: PreferredSize(
              preferredSize: const Size.fromHeight(50),
              child: Align(
                alignment: Alignment.centerLeft,
                child: AppBar(
                  elevation: 0,
                  backgroundColor: Colors.white,
                  bottom: TabBar(
                    padding: const EdgeInsets.only(bottom: 10),
                    labelPadding: const EdgeInsets.only(left: 0),
                    isScrollable: false,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: Colors.white,
                    labelColor: Colors.black,
                    unselectedLabelColor: Colors.grey,
                    tabs: [
                      InkWell(
                        child: Container(
                          width: 150,
                          height: 30,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10, top: 10),
                            child: Text(
                              'Following',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: currentScreen == 0
                                    ? Colors.black
                                    : Colors.grey,
                              ),
                              maxLines: 1,
                            ),
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            currentScreen = 0;
                          });
                        },
                      ),
                      InkWell(
                        child: Container(
                          width: 350,
                          height: 30,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 1, top: 10),
                            child: Text(
                              'Recommended',
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: currentScreen == 1
                                    ? Colors.black
                                    : Colors.grey,
                              ),
                              maxLines: 1,
                            ),
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            currentScreen = 1;
                          });
                        },
                      ),
                      const SizedBox(
                        width: 7,
                      ),
                      InkWell(
                        child: AbsorbPointer(
                          child: Container(
                            color: Colors.white,
                            width: 50,
                            padding: const EdgeInsets.only(left: 1),
                            child: const Icon(
                              Typicons.heart,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            currentScreen = 2;
                          });
                        },
                      )
                    ],
                  ),
                ),
              )),
          body: IndexedStack(
            index: currentScreen,
            children: screen,
          )),
    );
  }
}
