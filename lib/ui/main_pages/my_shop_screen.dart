import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/pages/edit_my_shop.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:flutter_fp/ui/style/text_style.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:video_player/video_player.dart';

import '../../bloc/cloud_storage_bloc.dart';
import '../components/product_card.dart';

class MyShopScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyShopScreenState();
  }
}

class MyShopScreenState extends State<MyShopScreen> {
  late CollectionReference users;
  late CollectionReference productsRef;
  late int _index;
  String shopName = "Example";
  @override
  void initState() {
    _index = 0;
    users = FirebaseFirestore.instance.collection('users');
    productsRef = FirebaseFirestore.instance.collection('products');

    super.initState();
  }

  void changeIndexedStack(index) {
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: getDocumentByIdAsStream('shops', getUid()),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data!.exists) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              body: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: IndexedStack(
                  children: [
                    MyShopHome(
                      changeIndexedStack: changeIndexedStack,
                      productsRef: productsRef,
                    ),
                    CreateProduct(
                      changeIndexedStack: changeIndexedStack,
                      productsRef: productsRef,
                    )
                  ],
                  index: _index,
                ),
              ),
            );
          } else {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
                elevation: 0,
              ),
              body: SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Open your Shop now, and dive into business ⭐",
                          style: superBigBlackText,
                        ),
                        Container(margin: EdgeInsets.only(top: 10)),
                        const Text(
                          "By click Open Shop, you will approve the agreement of Team2 organize on your shop ",
                        ),
                        Container(margin: EdgeInsets.only(top: 20)),
                        const Text(
                          "Choose your shop name",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.w500),
                        ),
                        Container(margin: EdgeInsets.only(top: 20)),
                        TextFormField(
                          // controller:
                          //     TextEditingController(text: name),

                          style: TextStyle(color: blackText),
                          initialValue: shopName,
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.left,
                          cursorColor: blackText,
                          decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 10, right: 10),
                            fillColor: const Color(0xFFE7ECF0),
                            filled: true,
                            border: OutlineInputBorder(
                                // gapPadding: 20,
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide.none),
                            hintText: 'e.g: Moon Shop',
                            //suffixIcon: const Icon(Icons.check),
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                          ),
                          onChanged: (String? value) {
                            shopName = value!;
                          },
                        ),
                        Container(margin: EdgeInsets.only(top: 10)),
                        getCustomButton("Create Shop", () {
                          createDocumentWithId('shops', getUid(), {
                            'active': true,
                            'name': shopName,
                            'address': "",
                            'tel': "",
                            'email': "",
                            'description': ""
                          });
                        }, Colors.green, Colors.white, 1)
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        } else {
          return const Text("");
        }
      },
    );
  }
}

class CreateProduct extends StatefulWidget {
  final void Function(int) changeIndexedStack;
  final CollectionReference productsRef;
  const CreateProduct(
      {Key? key, required this.changeIndexedStack, required this.productsRef})
      : super(key: key);

  @override
  State<CreateProduct> createState() => _CreateProductState();
}

class _CreateProductState extends State<CreateProduct> {
  final formKey = GlobalKey<FormState>();
  late String? name;
  late double? price;
  String? category = "Electronic";
  late String? description;
  List<File> imageFiles = [];
  List<File> videoFiles = [];
  // String? dropdownValue = "Electronic";
  late List<String> items = [];

  @override
  void initState() {
    FirebaseFirestore.instance.collection("catergories").get().then((snapshot) {
      for (var snap in snapshot.docs) {
        print('catergories:' + snap.id);
        setState(() {
          items.add(snap.id.toString());
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: 20,
        left: 20,
        right: 20,
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      widget.changeIndexedStack(0);
                    },
                    icon: Icon(
                      Icons.keyboard_arrow_right,
                      color: blackText,
                    )),
                const Text(
                  "Product",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                const Spacer(),
                CreateProduct(),
              ],
            ),
            Column(
              children: [
                Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.all(15),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 10, bottom: 5),
                          child: const Text(
                            "Name",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextFormField(
                          style: const TextStyle(color: Colors.blue),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.left,
                          cursorColor: Colors.blue,
                          decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 10, right: 10),
                            fillColor: const Color(0xFFE7ECF0),
                            filled: true,
                            border: OutlineInputBorder(
                                // gapPadding: 20,
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide.none),
                            hintText: 'Enter product name',
                            suffixIcon: const Icon(Icons.check),
                            hintStyle: const TextStyle(
                                color: Colors.grey, fontSize: 15),
                          ),
                          onChanged: (String? value) {
                            name = value;
                          },
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 10, bottom: 5, top: 10),
                          child: const Text(
                            "Price",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          ],
                          style: const TextStyle(color: Colors.blue),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.left,
                          cursorColor: Colors.blue,
                          decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 10, right: 10),
                            fillColor: const Color(0xFFE7ECF0),
                            filled: true,
                            border: OutlineInputBorder(
                                // gapPadding: 20,
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide.none),
                            hintText: 'Enter price',
                            suffixIcon: const Icon(Icons.check),
                            hintStyle: const TextStyle(
                                color: Colors.grey, fontSize: 15),
                          ),
                          onChanged: (String? value) {
                            price = double.parse(value!);
                          },
                        ),
                        Container(
                            margin: const EdgeInsets.only(
                                left: 10, bottom: 5, top: 10),
                            child: const Text(
                              "Category",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )),
                        // TextFormField(
                        //   style: TextStyle(color: Colors.blue),
                        //   textAlignVertical: TextAlignVertical.center,
                        //   textAlign: TextAlign.left,
                        //   cursorColor: Colors.blue,
                        //   decoration: InputDecoration(
                        //     contentPadding:
                        //         EdgeInsets.only(left: 10, right: 10),
                        //     fillColor: Color(0xFFE7ECF0),
                        //     filled: true,
                        //     border: OutlineInputBorder(
                        //         // gapPadding: 20,
                        //         borderRadius: BorderRadius.circular(20),
                        //         borderSide: BorderSide.none),
                        //     hintText: 'Enter category name',
                        //     suffixIcon: Icon(Icons.check),
                        //     hintStyle:
                        //         TextStyle(color: Colors.grey, fontSize: 15),
                        //   ),
                        //   onChanged: (String? value) {
                        //     category = value;
                        //   },
                        // ),
                        Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: const Color(0xFFE7ECF0),
                                borderRadius: BorderRadius.circular(20)),
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: category,
                              icon: const Icon(Icons.arrow_downward),
                              elevation: 16,
                              style: const TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                width: 0.0,
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  category = newValue;
                                });
                              },
                              items: items.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            )),

                        Container(
                            margin: const EdgeInsets.only(
                                left: 10, bottom: 5, top: 10),
                            child: const Text(
                              "Description",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )),
                        TextFormField(
                          style: const TextStyle(color: Colors.blue),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.left,
                          cursorColor: Colors.blue,
                          decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.only(left: 10, right: 10),
                            fillColor: const Color(0xFFE7ECF0),
                            filled: true,
                            border: OutlineInputBorder(
                                // gapPadding: 20,
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide.none),
                            hintText: 'Enter description',
                            suffixIcon: const Icon(Icons.check),
                            hintStyle: const TextStyle(
                                color: Colors.grey, fontSize: 15),
                          ),
                          onChanged: (String? value) {
                            description = value;
                          },
                        ),
                        getCustomButton("Choose Image", () async {
                          List<XFile>? pickedFiles =
                              await ImagePicker().pickMultiImage();

                          if (pickedFiles != null) {
                            setState(() {
                              for (var pickedFile in pickedFiles) {
                                imageFiles.add(File(pickedFile.path));
                              }
                            });
                          }
                        }, Colors.white, blackText, 1),
                        getCustomButton("Upload a video", () async {
                          XFile? pickedFile = await ImagePicker()
                              .pickVideo(source: ImageSource.camera);
                          if (pickedFile != null) {
                            setState(() {
                              videoFiles.add(File(pickedFile.path));

                              for (var video in videoFiles) {
                                print("oke" + video.path);
                              }
                            });
                          }
                        }, Colors.white, blackText, 1),
                        getCustomButton("Upload new image", () async {
                          XFile? pickedFile = await ImagePicker()
                              .pickImage(source: ImageSource.camera);
                          if (pickedFile != null) {
                            setState(() {
                              imageFiles.add(File(pickedFile.path));
                              // print("oke" + pickedFile.path);
                            });
                          }
                        }, Colors.white, blackText, 1)
                      ],
                    )),
              ],
            ),
            uploadedVideoList(),
            uploadedImagesList(),
          ],
        ),
      ),
    );
  }

  Widget uploadedVideoList() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: videoFiles.length,
        itemBuilder: (BuildContext context, int index) {
          if (videoFiles.isEmpty) {
            return const Text("No video");
          } else {
            // return Text(videoFiles[index].path);
            // print(videoFiles[index].statSync());
            // return Image.file(
            //   videoFiles[index],
            //   height: 200,
            // );
            var controller =
                VideoPlayerController.file(videoFiles[index].absolute);
            controller.initialize();
            controller.setLooping(true);
            controller.play();
            // return Text(videoFiles[index].path);
            return InkWell(
              child: AspectRatio(
                aspectRatio: controller.value.aspectRatio,
                child: VideoPlayer(
                  controller,
                  key: GlobalKey(),
                ),
              ),
              onTap: () {
                controller.play();
              },
            );
          }
        });
  }

  Widget uploadedImagesList() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: imageFiles.length,
        itemBuilder: (BuildContext context, int index) {
          if (imageFiles.isEmpty) {
            return const Text("No image");
          } else {
            // return Text(imageFiles[index].path);
            print(imageFiles[index].statSync());
            return Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              child: Image.file(
                imageFiles[index],
              ),
            );
          }
        });
  }

  Widget CreateProduct() {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color?>(Colors.white),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              //side: BorderSide(color: greyTweet, width: 0.5)
            ))),
        onPressed: () async {
          final storage = FirebaseStorage.instance;
          final storageRef = FirebaseStorage.instance.ref();
          final imagesRef = storageRef.child("images");
          // final spaceRef = storageRef.child("images/space.jpg");
          final metadata = SettableMetadata(contentType: "image/jpeg");
          // final appDocDir = await getApplicationDocumentsDirectory();
          // final filePath = "${appDocDir.absolute}/path/to/mountains.jpg";
          // final file = File(filePath);
          //   });
          // final uploadTask = storageRef
          //     .child("images/path/to/mountains.jpg")
          //     .putFile(file, metadata);
          int i = 0;
          // for (var file in imageFiles) {
          //   imagesRef
          //       .child(name! +
          //           "_" +
          //           description! +
          //           "_" +
          //           price!.toString() +
          //           "_" +
          //           i.toString())
          //       .putFile(file);
          //   i += 1;
          // }
// Listen for state changes, errors, and completion of the upload.
          // uploadTask.snapshotEvents.listen((TaskSnapshot taskSnapshot) {
          //   switch (taskSnapshot.state) {
          //     case TaskState.running:
          //       final progress = 100.0 *
          //           (taskSnapshot.bytesTransferred / taskSnapshot.totalBytes);
          //       print("Upload is $progress% complete.");
          //       break;
          //     case TaskState.paused:
          //       print("Upload is paused.");
          //       break;
          //     case TaskState.canceled:
          //       print("Upload was canceled");
          //       break;
          //     case TaskState.error:
          //       // Handle unsuccessful uploads
          //       break;
          //     case TaskState.success:
          //       // Handle successful uploads on complete
          //       // ...
          //       break;
          //   }
          // });
          widget.productsRef.add({
            'name': name, // John Doe
            'price': price, // Stokes and Sons
            'category': category,
            'description': description, // 42
            'seller': FirebaseAuth.instance.currentUser!.uid.toString(),
          }).then((value) {
            print(value.id);
            int i = 0;
            for (var img in imageFiles) {
              imagesRef.child(value.id).child(i.toString()).putFile(img);
              i += 1;
            }
            for (var video in videoFiles) {
              imagesRef.child(value.id).child(i.toString()).putFile(video);
              i += 1;
            }
          });

          widget.changeIndexedStack(0);

          print("name=$name");
          print("price=$price");
          print("category=$category");
          //   print('email=$eail');
          //   print('Demo only: password=$password');
          // }
        },
        child: const Text(
          'Create',
          style: TextStyle(color: Colors.black),
        ));
  }
}

class MyShopHome extends StatefulWidget {
  final CollectionReference productsRef;
  final void Function(int) changeIndexedStack;
  const MyShopHome(
      {Key? key, required this.changeIndexedStack, required this.productsRef})
      : super(key: key);

  @override
  State<MyShopHome> createState() => _MyShopHomeState();
}

class _MyShopHomeState extends State<MyShopHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          // direction: Axis.vertical,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                const BackButton(),
                const Text(
                  "My shop",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            getCustomButton("My shop information", () {
              Navigator.push(
                  context,
                  PageTransition(
                      child: const EditMyShop(),
                      type: PageTransitionType.rightToLeft));
            }, Colors.white, blackText, 1),
            ElevatedButton(
                style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double?>(0),
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide(color: primaryColor!, width: 1))),
                ),
                onPressed: () {
                  widget.changeIndexedStack(1);
                },
                child: Text(
                  "Create new product",
                  style: TextStyle(color: blackText),
                )),
            Text(
              "Your in stock items",
              style: TextStyle(
                  color: primaryColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            StreamBuilder<QuerySnapshot>(
              stream: widget.productsRef
                  .where('seller', isEqualTo: getUid())
                  .snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return const Text('Something went wrong');
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Text("Loading");
                }
                if (snapshot.hasData) {
                  return Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        // children: snapshot.data!.docs
                        //     .map((DocumentSnapshot document) {
                        //       Map<String, dynamic> data =
                        //           document.data()! as Map<String, dynamic>;
                        itemCount: snapshot.data!.docs.length,
                        itemBuilder: (context, index) {
                          return StreamBuilder<String>(
                              stream: getDownloadUrlAsStream("images/" +
                                  snapshot.data!.docs[index].id +
                                  "/0"),
                              builder: (context, imageSnapshot) {
                                if (imageSnapshot.hasData) {
                                  return getProductCard(
                                      NetworkImage(imageSnapshot.data!),
                                      snapshot.data!.docs[index]['name'],
                                      null,
                                      snapshot.data!.docs[index]['price'],
                                      snapshot.data!.docs[index].id,
                                      context);
                                } else {
                                  return getProductCard(
                                      AssetImage("assets/loading.gif"),
                                      snapshot.data!.docs[index]['name'],
                                      null,
                                      snapshot.data!.docs[index]['price'],
                                      snapshot.data!.docs[index].id,
                                      context);
                                }
                              });
                          // return ListTile(
                          //   title: Text(snapshot.data!.docs[index]['name']),
                          //   subtitle: Text(snapshot.data!.docs[index]['price']
                          //       .toString()),
                          // );
                        }),
                  );
                }
                return Text("");
              },
            )
          ],
        ),
      ),
    );
  }
}
