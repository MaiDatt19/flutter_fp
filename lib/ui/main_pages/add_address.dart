import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/style/colors.dart';

// import 'package:latlong/latlong.dart';
// import 'package:latlong/latlong.dart';

class AddAddressScreen extends StatefulWidget {
  const AddAddressScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddAddressScreenState();
  }
}

class AddAddressScreenState extends State<AddAddressScreen> {
  String city = "Ho Chi Minh city";
  List<String> cities = ["Ha Noi", "Ho Chi Minh city", "Da Nang"];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("Add address"),
          backgroundColor: tertiaryColor,
          elevation: 0,
          foregroundColor: blackText,
        ),
        body: Container(
            color: tertiaryColor,
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                        color: const Color(0xFFE7ECF0),
                        borderRadius: BorderRadius.circular(20)),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      value: city,
                      icon: const Icon(Icons.arrow_downward),
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        width: 0.0,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          city = newValue!;
                        });
                      },
                      items:
                          cities.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )),
                getCustomButton("Add new address", () {
                  createDocumentAutoId(
                      'addresses', {'uid': getUid(), 'address': city});
                  Navigator.pop(context);
                }, tertiaryColor, blackText, 1)
              ],
            )));
  }
}
