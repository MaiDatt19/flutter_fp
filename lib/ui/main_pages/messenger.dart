import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/pages/message.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:flutter_fp/ui/style/text_style.dart';
import 'package:intl/intl.dart';

import '../components/avatar.dart';

class MessagerScreen extends StatefulWidget {
  final void Function(int) changeIndexedStack;
  const MessagerScreen({Key? key, required this.changeIndexedStack})
      : super(key: key);

  @override
  State<MessagerScreen> createState() => _MessagerScreenState();
}

class _MessagerScreenState extends State<MessagerScreen> {
  bool isAlertClosed = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Messenger",
            style: TextStyle(color: blackText, fontSize: 18),
          ),
          elevation: 0,
          actions: [
            IconButton(
                onPressed: () => {},
                icon: ImageIcon(
                  const AssetImage("assets/phone_book.png"),
                  color: blackText,
                )),
            IconButton(
                onPressed: () => {},
                icon: ImageIcon(
                  const AssetImage("assets/box.png"),
                  color: blackText,
                )),
            IconButton(
                onPressed: () => {},
                icon: ImageIcon(
                  const AssetImage("assets/write.png"),
                  color: blackText,
                )),
            Container(
              margin: const EdgeInsets.only(left: 10),
            )
          ],
          backgroundColor: Colors.white,
          bottom: PreferredSize(
              child: isAlertClosed
                  ? Container()
                  : Container(
                      color: secondaryColor,
                      height: 50,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.notifications,
                              color: darkGrey,
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                            ),
                            Flexible(
                              child: EasyRichText(
                                "Don't miss important messages Turn On Notifications",
                                patternList: [
                                  EasyRichTextPattern(
                                    targetString: "Turn On Notifications",
                                    style: TextStyle(color: primaryColor),
                                  )
                                ],
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  setState(() {
                                    isAlertClosed = true;
                                  });
                                },
                                child: const Icon(Icons.close))
                          ],
                        ),
                      ),
                    ),
              preferredSize: Size.fromHeight(isAlertClosed ? 0 : 50)),
        ),
        body: Container(
          decoration: const BoxDecoration(color: Colors.white),
          child: Column(
            children: [
              if (FirebaseAuth.instance.currentUser != null)
                Container(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('conversations')
                        .where('people', arrayContains: getUid())
                        .orderBy('lastActiveTime', descending: true)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                        return Expanded(
                          child: ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context, index) {
                                return MessageTabItem(
                                    talkerId: snapshot.data!.docs[index]
                                                ['people'][0] ==
                                            getUid()
                                        ? snapshot.data!.docs[index]['people']
                                            [1]
                                        : snapshot.data!.docs[index]['people']
                                            [0],
                                    conversationId:
                                        snapshot.data!.docs[index].id);
                              }),
                        );
                      } else {
                        return Center(
                          child: Text("No conversations"),
                        );
                      }
                      // return StreamBuilder<QuerySnapshot>(
                      //     stream: FirebaseFirestore.instance
                      //         .collection('conversations')
                      //         .where('personTwoId', isEqualTo: getUid())
                      //         .snapshots(),
                      //     builder: (context, snapshot2) {
                      //       var conversationList = [];
                      //       // if (snapshot1.hasData || snapshot2.hasData) {
                      //       //   conversationList = [];
                      //       // }
                      //       if (snapshot1.hasData &&
                      //           snapshot1.data!.docs.isNotEmpty) {
                      //         for (var snapshot1Element
                      //             in snapshot1.data!.docs) {
                      //           conversationList.add(snapshot1Element);
                      //         }
                      //       }
                      //       if (snapshot2.hasData &&
                      //           snapshot2.data!.docs.isNotEmpty) {
                      //         for (var snapshot2Element
                      //             in snapshot2.data!.docs) {
                      //           conversationList.add(snapshot2Element);
                      //         }
                      //
                      //         conversationList.sort((a, b) {
                      //           return b['lastActiveTime']
                      //               .compareTo(a['lastActiveTime']);
                      //         });
                      //       }
                      //       if (conversationList.isNotEmpty) {
                      //         return Expanded(
                      //           child: ListView.builder(
                      //               itemCount: conversationList.length,
                      //               itemBuilder: (context, index) {
                      //                 return MessageTabItem(
                      //                     talkerId: getUid() ==
                      //                             conversationList[index]
                      //                                 ['personOneId']
                      //                         ? conversationList[index]
                      //                             ['personTwoId']
                      //                         : conversationList[index]
                      //                             ['personOneId'],
                      //                     conversationId:
                      //                         conversationList[index].id);
                      //               }),
                      //         );
                      //       } else {
                      //         return Center(
                      //           child: Text("No conversations"),
                      //         );
                      //       }
                      //     });
                      // if(snapshot.hasData) {
                      //   return ListView.builder(
                      //       itemCount: snapshot.data!.docs.length,
                      //       itemBuilder: (context, index) {
                      //
                      //         return MessageTabItem();
                      //
                      //
                      //       });
                      // }
                      // else{
                      //   return Center(
                      //     child: Text("No Conversation"),
                      //   )
                      // }
                    },
                  ),
                  // MessageTabItem(),
                  // MessageTabItem()
                )
            ],
          ),
        ));
  }
}

class MessageTabItem extends StatefulWidget {
  final String talkerId;
  final String conversationId;
  const MessageTabItem(
      {Key? key, required this.talkerId, required this.conversationId})
      : super(key: key);

  @override
  State<MessageTabItem> createState() => _MessageTabItemState();
}

class _MessageTabItemState extends State<MessageTabItem> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
        stream: FirebaseFirestore.instance
            .collection("shops")
            .doc(widget.talkerId)
            .snapshots(),
        builder: (context, shopSnapshot) {
          if (shopSnapshot.hasData) {
            return InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MessageScreen(
                              receiverId: widget.talkerId,
                            )));
              },
              child: Container(
                margin: const EdgeInsets.only(
                    left: 10, top: 14, bottom: 25, right: 15),
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection("messages")
                        .where('conversationId',
                            isEqualTo: widget.conversationId)
                        .orderBy('sendTime', descending: true)
                        .limit(1)
                        .snapshots(),
                    builder: (context, lastMessageSnapshot) {
                      if (lastMessageSnapshot.hasData &&
                          lastMessageSnapshot.data!.docs.isNotEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const AvatarBox(),
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          margin: const EdgeInsets.only(
                                              left: 15, bottom: 5),
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                left: 5.0),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.25,
                                            child: FittedBox(
                                              fit: BoxFit.fitWidth,
                                              child: Text(
                                                  shopSnapshot.data!['name'],
                                                  textAlign: TextAlign.left,
                                                  style:
                                                      getSmallProfileNameStyle()),
                                            ),
                                          )),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.3,
                                        child: FittedBox(
                                          fit: BoxFit.fitWidth,
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                left: 5.0, bottom: 5),
                                            child: Text(
                                                DateFormat.MMMd()
                                                    .add_jm()
                                                    .format(lastMessageSnapshot
                                                        .data!
                                                        .docs
                                                        .first['sendTime']
                                                        .toDate()),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: primaryColor,
                                                    fontSize: 15)),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                      margin: const EdgeInsets.only(left: 15),
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(left: 5.0),
                                        child: Text(
                                            lastMessageSnapshot
                                                .data!.docs.first['text']
                                                .toString(),
                                            overflow: TextOverflow.ellipsis,
                                            style: greyText),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        );
                      } else {
                        return const Text("loading");
                      }
                    }),
              ),
            );
          } else {
            return InkWell(
              onTap: () {
                print("kk");
              },
              child: Container(
                margin: const EdgeInsets.only(
                    left: 10, top: 14, bottom: 25, right: 15),
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection("messages")
                        .where('conversationId',
                            isEqualTo: widget.conversationId)
                        .orderBy('sendTime', descending: true)
                        .limit(1)
                        .snapshots(),
                    builder: (context, lastMessageSnapshot) {
                      if (lastMessageSnapshot.hasData) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const AvatarBox(),
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          margin: const EdgeInsets.only(
                                              left: 15, bottom: 5),
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                left: 5.0),
                                            child: Text("User",
                                                textAlign: TextAlign.left,
                                                style:
                                                    getSmallProfileNameStyle()),
                                          )),
                                      Container(
                                          margin:
                                              const EdgeInsets.only(left: 20),
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                left: 5.0),
                                            child: Text(
                                                lastMessageSnapshot.data!.docs
                                                    .first['sendTime']
                                                    .toDate()
                                                    .toString(),
                                                overflow: TextOverflow.ellipsis,
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: primaryColor,
                                                    fontSize: 15)),
                                          )),
                                    ],
                                  ),
                                  Container(
                                      margin: const EdgeInsets.only(left: 15),
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(left: 5.0),
                                        child: Text(
                                            lastMessageSnapshot
                                                .data!.docs.first['text']
                                                .toString(),
                                            overflow: TextOverflow.ellipsis,
                                            style: greyText),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        );
                      } else {
                        return getCircularProgressBar();
                      }
                    }),
              ),
            );
          }
        });
  }
}

class AvatarWithStatus extends StatefulWidget {
  const AvatarWithStatus({Key? key}) : super(key: key);

  @override
  State<AvatarWithStatus> createState() => _AvatarWithStatusState();
}

class _AvatarWithStatusState extends State<AvatarWithStatus> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Conversation extends StatefulWidget {
  const Conversation({Key? key}) : super(key: key);

  @override
  State<Conversation> createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

// class MessengerScreen extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return MessengerScreenState();
//   }
// }

// class MessengerScreenState extends State<MessengerScreen> {
//   DocumentReference user = FirebaseFirestore.instance
//       .collection("users")
//       .doc(FirebaseAuth.instance.currentUser!.uid.toString());
//   bool isTurnOnNotification = false;
//   bool isAlertClosed = false;
//   @override
//   void initState() {
//     isAlertClosed = isTurnOnNotification;
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "Messenger",
//           style: TextStyle(color: blackText, fontSize: 18),
//         ),
//         elevation: 0,
//         actions: [
//           IconButton(
//               onPressed: () => {},
//               icon: ImageIcon(
//                 AssetImage("assets/phone_book.png"),
//                 color: blackText,
//               )),
//           IconButton(
//               onPressed: () => {},
//               icon: ImageIcon(
//                 AssetImage("assets/box.png"),
//                 color: blackText,
//               )),
//           IconButton(
//               onPressed: () => {},
//               icon: ImageIcon(
//                 AssetImage("assets/write.png"),
//                 color: blackText,
//               )),
//           Container(
//             margin: const EdgeInsets.only(left: 10),
//           )
//         ],
//         backgroundColor: Colors.white,
//         bottom: PreferredSize(
//             child: isAlertClosed
//                 ? Container()
//                 : Container(
//                     color: lightGrey,
//                     height: 50,
//                     child: Padding(
//                       padding: const EdgeInsets.all(10.0),
//                       child: Row(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Icon(
//                             Icons.notifications,
//                             color: darkGrey,
//                           ),
//                           Container(
//                             margin: EdgeInsets.only(left: 10),
//                           ),
//                           Flexible(
//                             child: Container(
//                                 child: EasyRichText(
//                               "Don't miss important messages Turn On Notifications",
//                               patternList: [
//                                 EasyRichTextPattern(
//                                   targetString: "Turn On Notifications",
//                                   style: TextStyle(color: Colors.orange),
//                                 )
//                               ],
//                             )),
//                           ),
//                           InkWell(
//                               onTap: () {
//                                 setState(() {
//                                   isAlertClosed = true;
//                                 });
//                               },
//                               child: Icon(Icons.close))
//                         ],
//                       ),
//                     ),
//                   ),
//             preferredSize: Size.fromHeight(isAlertClosed ? 0 : 50)),
//       ),
//       body: Container(
//         color: Colors.white,
//         padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
//         child: Column(
//           children: [
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 InkWell(
//                   child: Column(
//                     children: [
//                       Image(
//                         width: 40,
//                         height: 40,
//                         image: AssetImage("assets/king.png"),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(bottom: 10),
//                       ),
//                       Text("Order")
//                     ],
//                   ),
//                 ),
//                 InkWell(
//                   child: Column(
//                     children: [
//                       Image(
//                         width: 40,
//                         height: 40,
//                         image: AssetImage("assets/tag.png"),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(bottom: 10),
//                       ),
//                       Text("Promotion")
//                     ],
//                   ),
//                 ),
//                 InkWell(
//                   onTap: () {},
//                   child: Column(
//                     children: [
//                       Image(
//                         width: 40,
//                         height: 40,
//                         image: AssetImage("assets/bell.png"),
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(bottom: 10),
//                       ),
//                       Text("Other")
//                     ],
//                   ),
//                 )
//               ],
//             ),
//             const Divider(),
//             const Spacer(),
//             Column(
//               children: [MessageTabItem(), MessageTabItem()],
//             ),
//             Text(
//               "Success in business start with a hello",
//               style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
//               textAlign: TextAlign.center,
//             ),
//             Container(
//               margin: EdgeInsets.only(bottom: 10),
//             ),
//             Text(
//               "Talk with supplier about pricing, customization, shipment or more",
//               style: TextStyle(
//                   fontSize: 15,
//                   fontWeight: FontWeight.normal,
//                   color: lightBlack),
//               textAlign: TextAlign.end,
//             ),
//             Container(
//               margin: EdgeInsets.only(bottom: 10),
//             ),
//             Image(
//               image: AssetImage("assets/conversation.png"),
//               width: 150,
//               height: 150,
//             ),
//             Container(
//               margin: EdgeInsets.only(bottom: 10),
//             ),
//             ElevatedButton(
//               onPressed: () {},
//               child: Text(
//                 "Source Now",
//                 style: TextStyle(color: lightBlack),
//               ),
//               style: ButtonStyle(
//                   elevation: MaterialStateProperty.all<double?>(0),
//                   backgroundColor:
//                       MaterialStateProperty.all<Color?>(Colors.white),
//                   shape: MaterialStateProperty.all<RoundedRectangleBorder>(
//                       RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(30.0),
//                           side: BorderSide(color: lightBlack, width: 0.5)))),
//             ),
//             const Spacer()
//           ],
//         ),
//       ),
//     );
//   }
// }

// class MessageTabItem extends StatefulWidget {
//   const MessageTabItem({Key? key}) : super(key: key);

//   @override
//   State<MessageTabItem> createState() => _MessageTabItemState();
// }

// class _MessageTabItemState extends State<MessageTabItem> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: 14, bottom: 25),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           AvatarBox(),
//           Flexible(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Container(
//                         margin: const EdgeInsets.only(left: 15),
//                         child: Container(
//                           margin: const EdgeInsets.only(left: 5.0),
//                           child: Text("Anya Forger",
//                               textAlign: TextAlign.left,
//                               style: getSmallProfileNameStyle()),
//                         )),
//                     Container(
//                         margin: const EdgeInsets.only(left: 20),
//                         child: Container(
//                           margin: const EdgeInsets.only(left: 5.0),
//                           child: Text("May 5",
//                               textAlign: TextAlign.left,
//                               style:
//                                   TextStyle(color: darkOrange, fontSize: 15)),
//                         )),
//                   ],
//                 ),
//                 Container(
//                     margin: const EdgeInsets.only(left: 15),
//                     child: Container(
//                       margin: const EdgeInsets.only(left: 5.0),
//                       child: Text("Hello how are you guys today!",
//                           style: greyText),
//                     )),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//     ;
//   }
// }
