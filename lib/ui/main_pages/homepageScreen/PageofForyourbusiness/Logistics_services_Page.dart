import 'package:flutter/material.dart';

class Logistics_services_Page extends StatefulWidget {
  const Logistics_services_Page({Key? key}) : super(key: key);

  @override
  State<Logistics_services_Page> createState() =>
      _Logistics_services_PageState();
}

class _Logistics_services_PageState extends State<Logistics_services_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: const Color(0xFFF5F5F5),
                hintStyle: const TextStyle(color: Color(0xFF959595)),
                hintText: "Search manufacturers",
                suffixIcon: const Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
