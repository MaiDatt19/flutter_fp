import 'package:flutter/material.dart';

class Request_Quotation_Page extends StatefulWidget {
  const Request_Quotation_Page({Key? key}) : super(key: key);

  @override
  State<Request_Quotation_Page> createState() => _Request_Quotation_PageState();
}

class _Request_Quotation_PageState extends State<Request_Quotation_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: const Color(0xFFF5F5F5),
                hintStyle: const TextStyle(color: Color(0xFF959595)),
                hintText: "Search manufacturers",
                suffixIcon: const Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
