import 'package:flutter/material.dart';

class Drop_Shipping_Page extends StatefulWidget {
  const Drop_Shipping_Page({Key? key}) : super(key: key);

  @override
  State<Drop_Shipping_Page> createState() => _Drop_Shipping_PageState();
}

class _Drop_Shipping_PageState extends State<Drop_Shipping_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: const Color(0xFFF5F5F5),
                hintStyle: const TextStyle(color: Color(0xFF959595)),
                hintText: "Search manufacturers",
                suffixIcon: const Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
