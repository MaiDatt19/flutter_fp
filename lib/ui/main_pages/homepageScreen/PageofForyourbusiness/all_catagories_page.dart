import 'package:flutter/material.dart';

class All_Categories_Page extends StatefulWidget {
  const All_Categories_Page({Key? key}) : super(key: key);

  @override
  State<All_Categories_Page> createState() => _All_Categories_PageState();
}

class _All_Categories_PageState extends State<All_Categories_Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: const Color(0xFFF5F5F5),
                hintStyle: const TextStyle(color: const Color(0xFF959595)),
                hintText: "Search manufacturers",
                suffixIcon: const Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
