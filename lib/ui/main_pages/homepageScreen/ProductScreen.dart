// import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/components/suggest_container.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Drop_Shipping_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Logistics_services_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Ready_to_Ship_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Request_Quotation_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Source_globally_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/all_catagories_page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/pageofNewarrivals/NewArrivalsPage.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:page_transition/page_transition.dart';

class ProductScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProductScreenState();
  }
}

class ProductScreenState extends State<ProductScreen> {
  final productsRef = FirebaseFirestore.instance.collection("products");
  final List<String> imageList = [];
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: tertiaryColor,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
                delegate: SliverChildListDelegate(<Widget>[
              Searh(),
              Foryourbusiness(),
              getSuggestContaier('Best sales', "", "", false, false, context),
              getSuggestContaier('New', "", "", true, false, context),
              // Just_for_you(),
            ])),
            // buildGridJustForYou()
          ],
        ),
      ),
    );
  }

  Widget Searh() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32),
                  borderSide: BorderSide.none),
              filled: true,
              fillColor: const Color(0xFFF5F5F5),
              hintStyle: const TextStyle(color: Color(0xFF959595)),
              hintText: "Search",
              suffixIcon: const Icon(Icons.search),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 30.0),
          ),
        ],
      ),
    );
  }

  Widget Foryourbusiness() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      InkWell(
        child: IntrinsicWidth(
          child: Container(
            width: 500,
            color: Colors.white,
            child: const ListTile(
              title: Text('For your',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ),
          ),
        ),
      ),
      Container(
        height: 100,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 100,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 241, 108, 31),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 241, 108, 31)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "All \ncategories",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const All_Categories_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 61, 40, 247),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 61, 40, 247)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Electronic",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Request_Quotation_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 211, 21, 148),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 211, 21, 148)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Ready \nto Ship",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Ready_to_Ship_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 164, 40, 247),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 164, 40, 247)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Source \nglobally",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Source_globally_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(106, 20, 185, 62),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(106, 20, 185, 62)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Logistics \nservices",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Logistics_services_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 186, 11, 240),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 186, 11, 240)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Drop \nShipping",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Drop_Shipping_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
          ],
        ),
      ),
    ]);
  }

  // Widget New_arrivals() {
  //   final imagesRef = storageRef.child("images");

  //   var productsRef = FirebaseFirestore.instance.collection('products');
  //   return Container(
  //       height: 500,
  //       child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: <Widget>[
  //             InkWell(
  //               child: IntrinsicWidth(
  //                 child: Container(
  //                   width: 500,
  //                   color: Colors.white,
  //                   child: ListTile(
  //                     title: Text('New arrivals',
  //                         style: TextStyle(
  //                             color: Colors.black,
  //                             fontSize: 20,
  //                             fontWeight: FontWeight.bold)),
  //                     trailing: Icon(
  //                       Icons.arrow_back_ios,
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //               onTap: () {
  //                 Navigator.push(
  //                     context,
  //                     PageTransition(
  //                         child: NewArrivalsPage(),
  //                         type: PageTransitionType.rightToLeft));
  //               },
  //             ),
  //             Container(
  //               height: 170,
  //               child: StreamBuilder<QuerySnapshot>(
  //                   stream: productsRef.snapshots(),
  //                   builder: (BuildContext context,
  //                       AsyncSnapshot<QuerySnapshot> snapshot) {
  //                     if (snapshot.hasError) {
  //                       return const Text('Something went wrong');
  //                     }

  //                     if (snapshot.connectionState == ConnectionState.waiting) {
  //                       return const Text("Loading");
  //                     }
  //                     return ListView(
  //                       shrinkWrap: true,
  //                       scrollDirection: Axis.horizontal,
  //                       children: snapshot.data!.docs
  //                           .map((DocumentSnapshot document) {
  //                             Map<String, dynamic> data =
  //                                 document.data()! as Map<String, dynamic>;
  //                             var imageRef =
  //                                 imagesRef.child(document.id).child("0");
  //                             print("id:" + document.id);

  //                             return FutureBuilder(
  //                               future: imageRef.getDownloadURL(),
  //                               builder: (BuildContext context, url) {
  //                                 if (url.hasData) {
  //                                   print("link:" + url.data.toString());
  //                                   return getProductCard(
  //                                       NetworkImage(url.data.toString()),
  //                                       data['name'],
  //                                       Text(""),
  //                                       data['price'],
  //                                       document.id);
  //                                 } else {
  //                                   return getProductCard(
  //                                       AssetImage("assets/loading.gif"),
  //                                       data['name'],
  //                                       Text(""),
  //                                       data['price'],
  //                                       document.id);
  //                                 }
  //                               },
  //                             );
  //                           })
  //                           .toList()
  //                           .cast(),
  //                     );
  //                   }),
  //             ),
  //           ]));
  // }

  // Widget Top_ranking() {
  //   final imagesRef = storageRef.child("images");

  //   var productsRef = FirebaseFirestore.instance.collection('products');
  //   return Container(
  //       child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: <Widget>[
  //         InkWell(
  //           child: IntrinsicWidth(
  //             child: Container(
  //               width: 500,
  //               color: Colors.white,
  //               child: ListTile(
  //                 title: Text('Top-ranking',
  //                     style: TextStyle(
  //                         color: Colors.black,
  //                         fontSize: 20,
  //                         fontWeight: FontWeight.bold)),
  //                 trailing: Icon(
  //                   Icons.arrow_back_ios,
  //                 ),
  //               ),
  //             ),
  //           ),
  //           onTap: () {
  //             Navigator.push(
  //                 context,
  //                 PageTransition(
  //                     child: NewArrivalsPage(),
  //                     type: PageTransitionType.rightToLeft));
  //           },
  //         ),
  //         Container(
  //           height: 170,
  //           child: StreamBuilder<QuerySnapshot>(
  //               stream: productsRef.snapshots(),
  //               builder: (BuildContext context,
  //                   AsyncSnapshot<QuerySnapshot> snapshot) {
  //                 if (snapshot.hasError) {
  //                   return const Text('Something went wrong');
  //                 }

  //                 if (snapshot.connectionState == ConnectionState.waiting) {
  //                   return const Text("Loading");
  //                 }
  //                 return ListView(
  //                   shrinkWrap: true,
  //                   scrollDirection: Axis.horizontal,
  //                   children: snapshot.data!.docs
  //                       .map((DocumentSnapshot document) {
  //                         Map<String, dynamic> data =
  //                             document.data()! as Map<String, dynamic>;
  //                         var imageRef =
  //                             imagesRef.child(document.id).child("0");
  //                         print("id:" + document.id);

  //                         return FutureBuilder(
  //                           future: imageRef.getDownloadURL(),
  //                           builder: (BuildContext context, url) {
  //                             if (url.hasData) {
  //                               print("link:" + url.data.toString());
  //                               return getProductCard(
  //                                   data, NetworkImage(url.data.toString()));
  //                             } else {
  //                               return getProductCard(
  //                                   data, AssetImage("assets/loading.gif"));
  //                             }
  //                           },
  //                         );
  //                       })
  //                       .toList()
  //                       .cast(),
  //                 );
  //               }),
  //         ),
  //       ]));
  // }

  Widget Just_for_you() {
    return Column(
      children: [
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              width: 500,
              color: Colors.white,
              child: const ListTile(
                title: Text('Just For You',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                trailing: Icon(
                  Icons.arrow_forward,
                ),
              ),
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    child: const NewArrivalsPage(),
                    type: PageTransitionType.rightToLeft));
          },
        ),
        // Container(
        //   child: CarouselSlider(
        //     options: CarouselOptions(
        //       enlargeCenterPage: true,
        //       enableInfiniteScroll: false,
        //       autoPlay: true,
        //     ),
        //     items: imageList
        //         .map((e) => ClipRect(
        //               child: Stack(
        //                 fit: StackFit.expand,
        //                 children: <Widget>[
        //                   Image.network(e,
        //                       width: 100, height: 200, fit: BoxFit.cover)
        //                 ],
        //               ),
        //             ))
        //         .toList(),
        //   ),
        // ),
      ],
    );
  }

  Widget buildGridJustForYou() {
    return SliverGrid(
        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
          return Container(
            width: 50,
            height: 50,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Colors.black,
            ),
            child: Image.asset('assets/loading.gif'),
          );
        }, childCount: 4),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisSpacing: 10, crossAxisSpacing: 10));
  }
}
