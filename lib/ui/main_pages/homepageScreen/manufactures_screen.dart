import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/all_catagories_page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Logistics_services_Page.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/PageofForyourbusiness/Request_Quotation_Page.dart';
import 'package:page_transition/page_transition.dart';

class ManufacturersScreen extends StatefulWidget {
  const ManufacturersScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ManufacturersScreenState();
  }
}

class ManufacturersScreenState extends State<ManufacturersScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            Searh(),
            Foryourbusiness(),
          ]))
        ],
      ),
    );
  }

  Widget Searh() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32),
                  borderSide: BorderSide.none),
              filled: true,
              fillColor: const Color(0xFFF5F5F5),
              hintStyle: const TextStyle(color: Color(0xFF959595)),
              hintText: "Search",
              suffixIcon: const Icon(Icons.search),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 30.0),
          ),
        ],
      ),
    );
  }

  Widget Foryourbusiness() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      InkWell(
        child: IntrinsicWidth(
          child: Container(
            width: 500,
            color: Colors.white,
            child: const ListTile(
              title: Text('For your business',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ),
          ),
        ),
      ),
      SizedBox(
        height: 100,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 100,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 241, 108, 31),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 241, 108, 31)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "All \ncategories",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const All_Categories_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(255, 61, 40, 247),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(255, 61, 40, 247)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Request for \nQuotation",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Request_Quotation_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              width: 200,
              height: 200,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color.fromARGB(106, 20, 185, 62),
                    side: const BorderSide(
                        width: 3, color: Color.fromARGB(106, 20, 185, 62)),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    padding: const EdgeInsets.all(20)),
                child: const Text(
                  "Logistics \nservices",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: const Logistics_services_Page(),
                          type: PageTransitionType.rightToLeft));
                },
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
