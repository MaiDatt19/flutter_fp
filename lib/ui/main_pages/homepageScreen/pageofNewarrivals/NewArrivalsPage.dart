import 'package:flutter/material.dart';

class NewArrivalsPage extends StatefulWidget {
  const NewArrivalsPage({Key? key}) : super(key: key);

  @override
  State<NewArrivalsPage> createState() => _NewArrivalsPageState();
}

class _NewArrivalsPageState extends State<NewArrivalsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: Color(0xFFF5F5F5),
                hintStyle: TextStyle(color: Color(0xFF959595)),
                hintText: "Search manufacturers",
                suffixIcon: Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
