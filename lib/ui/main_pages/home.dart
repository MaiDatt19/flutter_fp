import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/main_pages/homepageScreen/ProductScreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int index = 0;

  @override
  void initState() {
    // TODO: implement initState
    _tabController = TabController(initialIndex: 0, length: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 10,
          elevation: 0,
          backgroundColor: Colors.white,
          bottom: TabBar(
            isScrollable: true,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 3,
            indicatorColor: Colors.white,
            controller: _tabController,
            tabs: [
              InkWell(
                splashFactory: NoSplash.splashFactory,
                onTap: () {
                  setState(() {
                    index = 0;
                  });
                },
                child: SizedBox(
                  width: 130,
                  //color: Colors.red,
                  child: Tab(
                    child: Text(
                      "Products",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight:
                              index == 0 ? FontWeight.bold : FontWeight.normal),
                    ),
                  ),
                ),
              ),
              // InkWell(
              //   splashFactory: NoSplash.splashFactory,
              //   onTap: () {
              //     setState(() {
              //       index = 1;
              //     });
              //   },
              //   child: SizedBox(
              //     //color: Colors.red,
              //     width: 130,
              //     child: Tab(
              //       child: Text(
              //         "Manufacturers",
              //         style: TextStyle(
              //             color: Colors.black,
              //             fontSize: 15,
              //             fontWeight:
              //                 index == 1 ? FontWeight.bold : FontWeight.normal),
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
        body: IndexedStack(
          children: [
            ProductScreen(),
            // ManufacturersScreen(),
          ],
          index: index,
        ));
  }
}
