import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/cart_bloc.dart';
import 'package:flutter_fp/ui/main_pages/place_order.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:page_transition/page_transition.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CartScreenState();
  }
}

class CartScreenState extends State<CartScreen> {
  num quantity = 1;
  final list = ['hello', 'hi'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.grey),
        backgroundColor: Colors.white,
        title: const Text(
          'Shopping Cart',
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.headphones)),
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: const PlaceOrderScreen(),
                        type: PageTransitionType.rightToLeft));
              },
              icon: const Icon(Icons.payment))
        ],
      ),
      body: Container(
        color: tertiaryColor,
        child: Stack(
          children: [
            if (FirebaseAuth.instance.currentUser != null) getCartItems(context)
          ],
        ),
      ),
    );
  }

  // Widget buildEmptyCart() {
  //   return Container(
  //     height: 300,
  //     decoration: const BoxDecoration(
  //         image: DecorationImage(
  //             fit: BoxFit.fill,
  //             image: AssetImage(
  //                 'assets/SmartSelect_20220528-032716_Alibabacom.jpg'))),
  //   );
  // }

  // buildCustomerCart() {
  //   return SliverList(
  //       delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
  //     return Slidable(
  //       child: Card(
  //         elevation: 5,
  //         margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
  //         child: Container(
  //           padding: const EdgeInsets.all(10),
  //           child: Row(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             children: [
  //               Expanded(
  //                   child: ClipRRect(
  //                 child: const Image(
  //                   fit: BoxFit.fill,
  //                   image: NetworkImage(
  //                       'https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.6435-9/145709338_448094809887719_3246525799316186173_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=N_KcNrugyDgAX_80OPO&tn=Ir2UOatW1s2h7G0A&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT_KZkqFsx1Z9PpI4xWqaRq6I-JtZ_FE9YOxvAt3zLt_dA&oe=62BF6122'),
  //                 ),
  //                 borderRadius: BorderRadius.circular(20),
  //               )),
  //               Expanded(
  //                   flex: 6,
  //                   child: Container(
  //                     padding: const EdgeInsets.all(8),
  //                     child: Column(
  //                       mainAxisAlignment: MainAxisAlignment.center,
  //                       crossAxisAlignment: CrossAxisAlignment.center,
  //                       children: [
  //                         const Padding(
  //                           //product's name
  //                           padding: EdgeInsets.all(8),
  //                           child: Text(
  //                             'NGHIA DOAN ',
  //                             style: TextStyle(
  //                                 fontSize: 18, fontWeight: FontWeight.bold),
  //                             maxLines: 2,
  //                           ),
  //                         ),
  //                         Padding(
  //                             //product's shop
  //                             padding: const EdgeInsets.all(8),
  //                             child: Row(
  //                               children: [
  //                                 const Icon(
  //                                   Icons.store,
  //                                   color: Colors.black,
  //                                 ),
  //                                 Container(
  //                                   margin: const EdgeInsets.only(left: 10),
  //                                   child: Text(
  //                                     "Shopee MD",
  //                                     style: TextStyle(
  //                                         fontSize: 17, color: darkGrey),
  //                                   ),
  //                                 )
  //                               ],
  //                             )),
  //                         Padding(
  //                             //product price
  //                             padding: const EdgeInsets.all(8),
  //                             child: Row(
  //                               mainAxisSize: MainAxisSize.max,
  //                               children: [
  //                                 Icon(
  //                                   Icons.monetization_on,
  //                                   color: darkOrange,
  //                                 ),
  //                                 Container(
  //                                   margin: const EdgeInsets.only(left: 10),
  //                                   child: const Text(
  //                                     "1000",
  //                                     style: TextStyle(
  //                                         fontSize: 17, color: Colors.green),
  //                                   ),
  //                                 )
  //                               ],
  //                             )),
  //                       ],
  //                     ),
  //                   )),
  //               Center(
  //                 child: ElegantNumberButton(
  //                   initialValue: quantity,
  //                   minValue: 1,
  //                   maxValue: 100,
  //                   step: 1,
  //                   buttonSizeHeight: 30,
  //                   buttonSizeWidth: 30,
  //                   decimalPlaces: 0,
  //                   color: Colors.white,
  //                   onChanged: (value) async {
  //                     setState(() {
  //                       quantity = value;
  //                     });
  //                   },
  //                 ),
  //               )
  //             ],
  //           ),
  //         ),
  //       ),
  //       endActionPane: ActionPane(motion: const ScrollMotion(), children: [
  //         SlidableAction(
  //           flex: 2,
  //           onPressed: (context) {},
  //           backgroundColor: Colors.white,
  //           foregroundColor: darkOrange,
  //           icon: Icons.remove_red_eye,
  //           label: 'VIEW',
  //         ),
  //         SlidableAction(
  //           onPressed: (context) {},
  //           backgroundColor: const Color.fromARGB(255, 239, 209, 209),
  //           foregroundColor: Colors.red,
  //           icon: Icons.delete_outline,
  //         )
  //       ]),
  //     );
  //   }, childCount: 5));
  // }

  // Widget buildJustForYou() {
  //   return Container(
  //     padding: EdgeInsets.all(15),
  //     child: Row(
  //       children: [
  //         Text(
  //           'JUST FOR YOU',
  //           style: TextStyle(
  //               color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
  //         )
  //       ],
  //     ),
  //   );
  // }

//   Widget buildGridJustForYou() {
//     return SliverGrid(
//         delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
//           return Container(
//             color: Colors.red,
//             child: Column(
//               children: [Text(list[index]), Text('Price')],
//             ),
//           );
//         }, childCount: 2),
//         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//             crossAxisCount: 2, mainAxisSpacing: 10, crossAxisSpacing: 10));
//   }
// }
}
