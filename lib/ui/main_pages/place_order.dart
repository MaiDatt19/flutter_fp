import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_elegant_number_button/flutter_elegant_number_button.dart';
import 'package:flutter_fp/bloc/cloud_storage_bloc.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/bloc/price_bloc.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/main_pages/add_address.dart';
import 'package:flutter_fp/ui/profile_pages/myorders.dart';
import 'package:page_transition/page_transition.dart';

import '../style/colors.dart';

class PlaceOrderScreen extends StatefulWidget {
  const PlaceOrderScreen({Key? key}) : super(key: key);

  @override
  State<PlaceOrderScreen> createState() => _PlaceOrderScreenState();
}

class _PlaceOrderScreenState extends State<PlaceOrderScreen> {
  SumCalculateBloc sumCalculateBloc = SumCalculateBloc();
  @override
  num quantity = 1;
  bool hasAddress = false;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Place Order',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        foregroundColor: blackText,
        backgroundColor: tertiaryColor,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: tertiaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Address(),
              OrderPreviewList(context, sumCalculateBloc),
              Shippingdetails(),
              Shippingfee(),
              StreamBuilder<double>(
                  stream: sumCalculateBloc.resultStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          TotalPrice(snapshot.data!),
                          Submit(snapshot.data!)
                        ],
                      );
                    } else {
                      return const Text("Calculating");
                    }
                  }),

              // Totalfee(),
            ],
          ),
        ),
      ),
    );
  }

  Widget OrderPreviewList(context, sumCalculateBloc) {
    List<double> sums = [];
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance
          .collection('cartItems')
          .where('buyerId', isEqualTo: getUid())
          .snapshots(),
      builder: (context, cartItemSnapshot) {
        if (cartItemSnapshot.hasData) {
          return ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: cartItemSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                return StreamBuilder<DocumentSnapshot>(
                    stream: getDocumentByIdAsStream('products',
                        cartItemSnapshot.data!.docs[index]['productId']),
                    builder: (context, productSnapshot) {
                      if (productSnapshot.hasData) {
                        double sum = (productSnapshot.data!['price'] *
                            cartItemSnapshot.data!.docs[index]['quantity']);
                        sums.add(sum);
                        sumCalculateBloc.priceCalculate.add(sums);
                        return ListTile(
                          leading: StreamBuilder<String>(
                            stream: getDownloadUrlAsStream(
                                'images/' + productSnapshot.data!.id + "/0"),
                            builder: (context, urlSnapshot) {
                              if (urlSnapshot.hasData) {
                                return CachedNetworkImage(
                                  imageUrl: urlSnapshot.data!.toString(),
                                  width:
                                      MediaQuery.of(context).size.width * 0.1,
                                  height:
                                      MediaQuery.of(context).size.width * 0.1,
                                );
                              } else {
                                return const Image(
                                  image: AssetImage("assets/loading.gif"),
                                );
                              }
                            },
                          ),
                          title: Text(productSnapshot.data!['name'].toString()),
                          subtitle: Text(
                            "\$" +
                                productSnapshot.data!['price'].toString() +
                                " X " +
                                cartItemSnapshot.data!.docs[index]['quantity']
                                    .toString(),
                            style: TextStyle(
                                color: blackText, fontWeight: FontWeight.bold),
                          ),
                          trailing: Text(
                            "\$" + sum.toString(),
                            style: TextStyle(
                                color: blackText, fontWeight: FontWeight.bold),
                          ),
                        );
                      } else {
                        return Center(
                          child: getCircularProgressBar(),
                        );
                      }
                    });
              });
        } else {
          return Center(
            child: getCircularProgressBar(),
          );
        }
      },
    );
  }

  Widget Address() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('addresses')
            .where('uid', isEqualTo: getUid())
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
            return Container(
              color: secondaryColor,
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IntrinsicWidth(
                    child: Container(
                      width: 500,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                      ),
                      child: ListTile(
                        title: Text(snapshot.data!.docs[0]['address'],
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                        trailing: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Container(
              color: secondaryColor,
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    child: IntrinsicWidth(
                      child: Container(
                        width: 500,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                        ),
                        child: const ListTile(
                          title: Text('Add shipping address',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold)),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          PageTransition(
                              child: const AddAddressScreen(),
                              type: PageTransitionType.rightToLeft));
                    },
                  ),
                ],
              ),
            );
          }
        });
  }

  Widget Productd_details() {
    return SizedBox(
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: const ListTile(
                  title: Text('Product details',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Soldby() {
    return SizedBox(
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                width: 500,
                color: Colors.white,
                child: const ListTile(
                  title: Text('Sold by: ',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Product() {
    return Container(
      child: Card(
        elevation: 5,
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: const Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.6435-9/145709338_448094809887719_3246525799316186173_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=N_KcNrugyDgAX_80OPO&tn=Ir2UOatW1s2h7G0A&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT_KZkqFsx1Z9PpI4xWqaRq6I-JtZ_FE9YOxvAt3zLt_dA&oe=62BF6122'),
                ),
                borderRadius: BorderRadius.circular(20),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: const EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Text(
                                "Chỗ này để miêu tả homie",
                                style: TextStyle(fontSize: 17, color: darkGrey),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Price() {
    return Container(
      decoration: BoxDecoration(
          color: const Color.fromARGB(255, 244, 234, 234),
          borderRadius: BorderRadius.circular(20)),
      // height: 96,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Row(
              children: [
                Expanded(
                  child: Text('USD 0.03',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
                Center(
                  child: ElegantNumberButton(
                    initialValue: quantity,
                    minValue: 1,
                    maxValue: 100,
                    step: 1,
                    buttonSizeHeight: 30,
                    // buttonSizeWidth: 30,
                    decimalPlaces: 0,
                    color: Colors.white,
                    onChanged: (value) async {
                      setState(() {
                        quantity = value;
                      });
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget TotalPrice(value) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              color: Colors.white,
              child: const ListTile(
                title: Text(
                  'Product price: ',
                  style: TextStyle(
                    color: Color.fromARGB(255, 159, 157, 157),
                    fontSize: 15,
                  ),
                ),
              ),
            ),
          ),
        ),
        const Spacer(),
        Container(
          padding: const EdgeInsets.all(22),
          child: Text(
            "\$" + value.toString(),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget Shippingdetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              color: Colors.white,
              child: const ListTile(
                title: Text('Shipping details',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget Shippingfee() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              color: Colors.white,
              child: const ListTile(
                title: Text('Shipping fee',
                    style: TextStyle(
                        color: Color.fromARGB(255, 159, 157, 157),
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ),
        ),
        const Spacer(),
        Container(
            padding: const EdgeInsets.all(22),
            child: const Text(
              "USD: 30.00",
              style: TextStyle(fontWeight: FontWeight.bold),
            ))
      ],
    );
  }

  Widget Totalfee() {
    return SizedBox(
      width: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: const ListTile(
                  title: Text('Total',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          const Spacer(),
          Container(
            padding: const EdgeInsets.all(22),
            child: const Text(
              "USD: 30.00",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget Submit(value) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        InkWell(
          child: IntrinsicWidth(
            child: Container(
              color: Colors.white,
              child: ListTile(
                title: Text('Total: \$' + (value + 30).toString(),
                    style: TextStyle(
                        color: blackText,
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ),
        ),
        const Spacer(),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: IntrinsicWidth(
              child: Container(
                padding: const EdgeInsets.all(10),
                width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(23),
                    color: primaryColor),
                child: Center(
                  child: Text(
                    "Place order",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: tertiaryColor),
                  ),
                ),
              ),
            ),
          ),
          onTap: () {
            var orderedItems = [];
            FirebaseFirestore.instance
                .collection('cartItems')
                .where('buyerId', isEqualTo: getUid())
                .get()
                .then((value) {
              for (var item in value.docs) {
                print(item['productId']);
                orderedItems.add(item.data());
                FirebaseFirestore.instance
                    .collection('cartItems')
                    .doc(item.id)
                    .delete();
              }

              // Create orders

              createDocumentAutoId('orders', {
                'orderedItems': orderedItems,
                'buyerId': getUid().toString(),
                'status': 'waiting confirm',
                'time': DateTime(
                  DateTime.now().year,
                  DateTime.now().month,
                  DateTime.now().day,
                  DateTime.now().hour,
                  DateTime.now().minute,
                )
              });
            });

            FirebaseFirestore.instance
                .collection('addresses')
                .where('uid', isEqualTo: getUid())
                .limit(1)
                .get()
                .then((value) {
              if (value.size != 0) {
                Navigator.push(
                    context,
                    PageTransition(
                        child: MyOrdersScreen(),
                        type: PageTransitionType.rightToLeft));
              } else {
                Navigator.push(
                    context,
                    PageTransition(
                        child: AddAddressScreen(),
                        type: PageTransitionType.rightToLeft));
              }
            });
          },
        ),
      ],
    );
  }
}
