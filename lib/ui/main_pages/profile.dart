import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/main_pages/my_shop_screen.dart';
import 'package:flutter_fp/ui/profile_pages/myorders.dart';
import 'package:flutter_fp/ui/profile_pages/qrscanner.dart';
import 'package:flutter_fp/ui/profile_pages/settings.dart';
import 'package:flutter_fp/ui/startup.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:page_transition/page_transition.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileScreenState();
  }
}

class ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            buildContainer1(),
            buildSpace(),
            buildContainer2(),
            buildSpace(),
            buildContainer3(),
            buildSpace(),
            buildContainer4(),
          ]))
        ],
      ),
    );
  }

  Widget buildContainer1() {
    return Container(
      padding:
          EdgeInsets.fromLTRB(20, MediaQuery.of(context).padding.top, 10, 10),
      child: Column(
        children: [
          Row(
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        PageTransition(
                            child: const QRViewExample(),
                            type: PageTransitionType.rightToLeft));
                  },
                  icon: const Icon(Icons.qr_code)),
              const Spacer(),
              const Text('USD'),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        PageTransition(
                            child: SettingsScreen(),
                            type: PageTransitionType.rightToLeft));
                  },
                  icon: const Icon(Icons.settings))
            ],
          ),
          Row(
            children: [
              CircleAvatar(
                backgroundColor: const Color.fromARGB(255, 224, 224, 224),
                child: ClipOval(
                  child: Icon(
                    Icons.person,
                    color: blackText,
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              if (FirebaseAuth.instance.currentUser == null)
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    // Foreground color
                    onPrimary: Colors.white,
                    // Background color
                    primary: primaryColor,
                  ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const StartUpScreen(),
                      ),
                    );
                  },
                  child: const Text('Sign In or Register'),
                )
              else
                Text(FirebaseAuth.instance.currentUser!.email!)
            ],
          )
        ],
      ),
    );
  }

  Widget buildContainer2() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // InkWell(
          //   child: Row(
          //     children: [
          //       Icon(
          //         Icons.list,
          //         color: blackText,
          //       ),
          //       Text(
          //         '  Xem 1 sản phẩm chi tiết',
          //         style: TextStyle(color: blackText, fontSize: 18),
          //       )
          //     ],
          //   ),
          //   onTap: () {
          //     Navigator.push(
          //         context,
          //         PageTransition(
          //             child: ProductDetailScreen(),
          //             type: PageTransitionType.bottomToTop));
          //   },
          // ),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.list,
                  color: blackText,
                ),
                Text(
                  '  My Orders',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      child: MyOrdersScreen(),
                      type: PageTransitionType.rightToLeft));
            },
          ),
          if (FirebaseAuth.instance.currentUser != null)
            const SizedBox(height: 20),
          if (FirebaseAuth.instance.currentUser != null)
            InkWell(
              child: Row(
                children: [
                  Icon(
                    Icons.upload,
                    color: blackText,
                  ),
                  Text(
                    ' My shop',
                    style: TextStyle(color: blackText, fontSize: 18),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyShopScreen()));
              },
            ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.favorite_border_outlined,
                  color: blackText,
                ),
                Text(
                  '  My Favorites',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.star_border_outlined,
                  color: blackText,
                ),
                Text(
                  '  Following',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.history,
                  color: blackText,
                ),
                Text(
                  '  Browsing History',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          // const SizedBox(height: 20),
          // InkWell(
          //   child: Row(
          //     children: [
          //       Icon(
          //         Icons.question_answer,
          //         color: blackText,
          //       ),
          //       Text(
          //         '  Request for Quotation',
          //         style: TextStyle(color: blackText, fontSize: 18),
          //       )
          //     ],
          //   ),
          //   onTap: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => MyOrdersScreen()));
          //   },
          // ),
          // const SizedBox(height: 20),
          // InkWell(
          //   child: Row(
          //     children: [
          //       Icon(
          //         Icons.mail_outline,
          //         color: blackText,
          //       ),
          //       Text(
          //         '  Inquiries',
          //         style: TextStyle(color: blackText, fontSize: 18),
          //       )
          //     ],
          //   ),
          //   onTap: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => MyOrdersScreen()));
          //   },
          // ),
        ],
      ),
    );
  }

  Widget buildContainer3() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.note,
                  color: blackText,
                ),
                Text(
                  '  Sourcing Preferences',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.discount,
                  color: blackText,
                ),
                Text(
                  '  My Coupons',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
        ],
      ),
    );
  }

  Widget buildContainer4() {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.maps_home_work_rounded,
                  color: blackText,
                ),
                Text(
                  '  Shipping Address',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.feedback_outlined,
                  color: blackText,
                ),
                Text(
                  '  App Feedback',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.question_mark,
                  color: blackText,
                ),
                Text(
                  '  Help Center',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyOrdersScreen()));
            },
          ),
          const SizedBox(height: 20),
          InkWell(
            child: Row(
              children: [
                Icon(
                  Icons.settings,
                  color: blackText,
                ),
                Text(
                  '  Settings',
                  style: TextStyle(color: blackText, fontSize: 18),
                )
              ],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      child: SettingsScreen(),
                      type: PageTransitionType.rightToLeft));
            },
          ),
        ],
      ),
    );
  }

  Widget buildSpace() {
    return SizedBox(
        height: 10,
        child: Container(
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 230, 230, 230))));
  }
}
