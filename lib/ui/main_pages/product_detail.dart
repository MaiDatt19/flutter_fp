// import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_elegant_number_button/flutter_elegant_number_button.dart';
import 'package:flutter_fp/UI/main_pages/cart.dart';
import 'package:flutter_fp/bloc/firestore_bloc.dart';
import 'package:flutter_fp/ui/components/buttons.dart';
import 'package:flutter_fp/ui/components/progress.dart';
import 'package:flutter_fp/ui/pages/message.dart';
import 'package:flutter_fp/ui/startup.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:page_transition/page_transition.dart';

import '../pages/shop_profile.dart';

class ProductDetailScreen extends StatefulWidget {
  // final void Function(int) changeIndexedStack;
  final String id;
  const ProductDetailScreen({required this.id});
  @override
  State<StatefulWidget> createState() {
    return ProductDetailScreenState();
  }
}

class ProductDetailScreenState extends State<ProductDetailScreen> {
  final List<CachedNetworkImage> imageList = [];
  num quantity = 1;

  String title = "";

  num sum = 0;

  var price;

  var description;

  var sellerId;

  @override
  void initState() {
    // TODO: implement initState
    // FirebaseFirestore.instance
    //     .collection("products")
    //     .doc(widget.id)
    //     .get()
    //     .then((snapshot) {
    //   // print(snapshot.data());
    //   var data = snapshot.data() as Map<String, dynamic>;
    //   setState(() {
    //     print(data);
    //     title = data['name'];
    //     price = data['price'];
    //     sum = data['price'];
    //     sellerId = data['seller'];
    //
    //     description = data['description'];
    //   });
    // });
    FirebaseStorage.instance
        .ref()
        .child("images")
        .child(widget.id)
        .listAll()
        .then((listResult) async {
      // print(value.items[0]);
      for (var item in listResult.items) {
        var imageUrl = await item.getDownloadURL();

        imageList.add(CachedNetworkImage(
          fit: BoxFit.fill,
          imageUrl: imageUrl,
        ));
      }
      setState(() {
        imageList;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.white,
        backgroundColor: Colors.white,
        foregroundColor: blackText,
        elevation: 1,
        // centerTitle: true,
        title: Text(
          "Item",
          style: TextStyle(color: blackText),
        ),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.more_vert))],
      ),
      extendBodyBehindAppBar: false,
      body: Container(
        color: tertiaryColor,
        child: SingleChildScrollView(
          child: StreamBuilder<DocumentSnapshot>(
              stream: getDocumentByIdAsStream('products', widget.id),
              builder: (context, productSnapshot) {
                // price = productSnapshot.data!['price'];
                // sum = quantity * price;
                if (productSnapshot.hasData) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 300,
                        child: Swiper.children(
                          containerHeight: 200,
                          children: imageList,
                          // itemCount: imageList.length,
                          loop: false,

                          pagination: SwiperPagination(),
                          control: SwiperControl(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          productSnapshot.data!['name'],
                          style: const TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Row(
                            children: [
                              Icon(
                                Icons.monetization_on,
                                color: primaryColor,
                              ),
                              Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: Text(
                                  "\$" +
                                      productSnapshot.data!['price'].toString(),
                                  style: TextStyle(
                                      color: lightBlack,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              ElegantNumberButton(
                                initialValue: quantity,
                                minValue: 1,
                                maxValue: 9999,
                                step: 1,
                                buttonSizeHeight: 40,
                                buttonSizeWidth: 40,
                                decimalPlaces: 0,
                                color: lightGrey,
                                onChanged: (value) async {
                                  setState(() {
                                    quantity = value;
                                    sum = productSnapshot.data!['price'] *
                                        quantity;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ),

                      // Padding(
                      //   padding: const EdgeInsets.all(20),
                      //   child: Column(
                      //     mainAxisAlignment: MainAxisAlignment.start,
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //
                      //     ],
                      //   ),
                      // ),
                      Card(
                        elevation: 5,
                        child: Container(
                            margin: const EdgeInsets.only(
                                top: 20, left: 20, right: 20, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    getCustomButton("Add to cart", () {
                                      if (FirebaseAuth.instance.currentUser ==
                                          null) {
                                        Navigator.push(
                                            context,
                                            PageTransition(
                                                child: const StartUpScreen(),
                                                alignment: Alignment.center,
                                                type:
                                                    PageTransitionType.scale));
                                      } else {
                                        FirebaseFirestore.instance
                                            .collection("cartItems")
                                            .add({
                                          'active': true,
                                          'productId': widget.id,
                                          'quantity': quantity,
                                          'buyerId': FirebaseAuth
                                              .instance.currentUser!.uid
                                              .toString()
                                        }).then((doc) {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  child: const CartScreen(),
                                                  alignment: Alignment.center,
                                                  type: PageTransitionType
                                                      .rightToLeftWithFade));
                                        });
                                      }
                                    }, primaryColor, Colors.white, 2),
                                    // Container(
                                    //     margin: EdgeInsets.only(right: 20)),
                                  ],
                                ),
                                Container(margin: EdgeInsets.only(top: 10)),
                                const Divider(),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            child: ShopProfile(
                                                id: productSnapshot
                                                    .data!['seller']),
                                            alignment: Alignment.center,
                                            type: PageTransitionType
                                                .rightToLeftWithFade));
                                  },
                                  child: StreamBuilder<DocumentSnapshot>(
                                      stream: getDocumentByIdAsStream('shops',
                                          productSnapshot.data!['seller']),
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState !=
                                                ConnectionState.waiting
                                            // &&
                                            // snapshot.data!['name'] != null &&
                                            // snapshot.data!['address'] != null
                                            ) {
                                          // print('name:' + snapshot.data!['name']);

                                          return ListTile(
                                            leading: CircleAvatar(
                                              backgroundColor: lightGrey,
                                              child: ClipOval(
                                                child: Icon(
                                                  Icons.person,
                                                  color: Colors.grey,
                                                ),
                                              ),
                                            ),
                                            title: Text(
                                              snapshot.data!['name'],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            subtitle:
                                                Text(snapshot.data!['address']),
                                            trailing: Container(
                                              child: IconButton(
                                                  onPressed: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                MessageScreen(
                                                                  receiverId: productSnapshot
                                                                          .data![
                                                                      'seller'],
                                                                )));
                                                  },
                                                  icon: ImageIcon(
                                                    AssetImage(
                                                        "assets/chat.png"),
                                                    color: blackText,
                                                  )),
                                            ),
                                          );
                                        } else {
                                          return SizedBox(
                                            height: 30,
                                            child: AspectRatio(
                                              aspectRatio: 0.3,
                                              child: getCircularProgressBar(),
                                            ),
                                          );
                                        }
                                      }),
                                ),
                                const Divider(),
                                Container(margin: EdgeInsets.only(top: 10)),
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    "Description",
                                    style: TextStyle(
                                        color: blackText, fontSize: 18),
                                  ),
                                ),
                                RichText(
                                    text: TextSpan(
                                        style: TextStyle(
                                            color: darkGrey, fontSize: 16),
                                        text: productSnapshot
                                            .data!['description'])),
                              ],
                            )),
                      )
                    ],
                  );
                } else {
                  return Center(
                    child: getCircularProgressBar(),
                  );
                }
              }),
        ),
      ),
    );
  }
}
