import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/main_pages/home.dart';
import 'package:flutter_fp/ui/main_pages/messenger.dart';
import 'package:flutter_fp/ui/main_pages/profile.dart';
import 'package:flutter_fp/ui/main_pages/trueview.dart';

import '../UI/main_pages/cart.dart';
import 'style/colors.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AppHomePage(),
    );
  }
}

class AppHomePage extends StatefulWidget {
  const AppHomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }
}

class AppHomePageState extends State<AppHomePage> {
  int currentIndex = 0;
  void changeIndexedStack(index) {
    setState(() {
      currentIndex = index;
    });
  }

  late List<Widget> screens;
  @override
  void initState() {
    screens = [
      const HomeScreen(),
      const TrueViewScreen(),
      MessagerScreen(changeIndexedStack: changeIndexedStack),
      const CartScreen(),
      ProfileScreen()
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        //-> switch giua cac trang
        index: currentIndex,
        children: screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          //=> bottom navigation bar
          type: BottomNavigationBarType.fixed,
          iconSize: 30,
          backgroundColor: Colors.white,
          selectedItemColor: primaryColor,
          unselectedItemColor: mediumGrey,
          enableFeedback: false,
          selectedFontSize: 15,
          unselectedFontSize: 15,
          selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w500),
          unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.w500),
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          currentIndex: currentIndex,
          items: const [
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.all(9.0),
                child: ImageIcon(AssetImage("assets/home.png")),
              ),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.all(9.0),
                child: ImageIcon(AssetImage("assets/heart_solid.png")),
              ),
              label: "Follow",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.all(9.0),
                child: ImageIcon(AssetImage("assets/message.png")),
              ),
              label: "Messenger",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.all(9.0),
                child: Icon(Icons.shopping_cart),
              ),
              label: "Cart",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.all(9.0),
                child: Icon(Icons.person),
              ),
              label: "My Profile",
            ),
          ]),
    );
  }
}
