import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/ui/app.dart';
import 'package:flutter_fp/ui/main_pages/home.dart';

import '../UI/sign_up.dart';
// import '../bloc/bloc.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignInScreenState();
  }
}

class SignInScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;
  // final bloc = Bloc();
  String emailValidate = "";
  late String emailErrorText = "";
  late String passwordErrorText = "";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        centerTitle: true,
        title: const Image(
          image: AssetImage('assets/logo.png'),
          width: 27,
          height: 27,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Enter your account",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 28,
                  fontWeight: FontWeight.bold),
            ),
            Form(
              key: formKey,
              child: Column(
                children: [
                  emailField(),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                  ),
                  passwordField(),
                  Container(
                    margin: const EdgeInsets.only(top: 40.0),
                  ),
                ],
              ),
            ),
            const Spacer(),
            const Divider(),
            Row(
              children: [
                Spacer(),
                SignInButton(),
              ],
            ),
            // GoToSignUpButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        // icon: const Icon(Icons.person),
        hintText: "Email",
        // errorText: emailErrorText,
      ),
      // validator: validateEmail,
      onChanged: (value) {
        // print(value);
        // bloc.changedEmail(value);
        // if (!value.contains("@")) {
        //   emailValidate = "invalid email";
        // } else {
        //   emailValidate = "";
        // }
        setState(() {
          emailErrorText = "";
        });
      },
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        // icon: const Icon(Icons.password),
        // labelText: 'Password',
        hintText: "Password",
        // errorText: passwordErrorText),
      ),
      // validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
      // onChanged: (value) {
      //   setState(() {
      //     emailErrorText = "";
      //   });
      // },
    );
  }

  // Widget DateField() {
  //   return TextFormField(
  //     obscureText: true,
  //     keyboardType: TextInputType.number,
  //     inputFormatters: <TextInputFormatter>[
  //       FilteringTextInputFormatter.digitsOnly,
  //     ],
  //     decoration: InputDecoration(
  //         icon: Icon(Icons.calendar_today), labelText: 'Year of birth'),
  //     validator: (value) {
  //       if (value!.length < 4) {
  //         return "Years has at least 4 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  // Widget NameField() {
  //   return TextFormField(
  //     obscureText: true,
  //     decoration:
  //         InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
  //     validator: (value) {
  //       if (value!.length < 1) {
  //         return "Name has at least 1 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  // Widget AddressField() {
  //   return TextFormField(
  //     decoration: InputDecoration(
  //         icon: Icon(Icons.location_city_sharp), labelText: "Address"),
  //     validator: validateAddress,
  //     onSaved: (value) {
  //       print('onSaved: value=$value');
  //     },
  //   );
  // }

  // Widget PasswordField() {
  //   return TextFormField(
  //     obscureText: true,
  //     decoration:
  //         InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
  //     validator: (value) {
  //       if (value!.length < 1) {
  //         return "Name has at least 1 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  Widget SignInButton() {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color?>(Colors.orange[400]),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              //side: BorderSide(color: greyTweet, width: 0.5)
            ))),
        onPressed: () async {
          if (formKey.currentState!.validate()) {
            // If the form is valid, display a snackbar. In the real world,
            // you'd often call a server or save the information in a database.
            formKey.currentState!.save();

            try {
              UserCredential userCredential = await FirebaseAuth.instance
                  .signInWithEmailAndPassword(email: email, password: password);
            } on FirebaseAuthException catch (e) {
              if (e.code == 'user-not-found') {
                emailErrorText = 'No user found for that email.';
              } else if (e.code == 'wrong-password') {
                passwordErrorText = 'Wrong password provided for that user.';
                print(passwordErrorText);
              }
            }
            FirebaseAuth.instance.authStateChanges().listen((User? user) {
              if (user == null) {
                print('User is currently signed out!');
              } else {
                print("oke");
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AppHomePage(),
                  ),
                );
              }
            });

            print('email=$email');
            print('Demo only: password=$password');
          }
        },
        child: const Text('Sign in'));
  }

  Widget GoToSignUpButton() {
    return ElevatedButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => const SignUpScreen(),
            ),
          );
        },
        child: const Text("Create account"));
  }
}
