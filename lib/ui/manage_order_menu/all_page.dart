import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fp/bloc/fireauth_bloc.dart';
import 'package:flutter_fp/ui/components/suggest_container.dart';
import 'package:flutter_fp/ui/style/colors.dart';
import 'package:intl/intl.dart';

class AllPage extends StatefulWidget {
  const AllPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AllPageState();
  }
}

class AllPageState extends State<AllPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      color: Colors.white,
      child: Column(
        children: [
          StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance
                  .collection('orders')
                  .where('buyerId', isEqualTo: getUid())
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                  return SizedBox(
                    height: 300,
                    child: ListView.builder(
                        itemCount: snapshot.data!.docs.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Card(
                              color: tertiaryColor,
                              child: ListTile(
                                leading: Text(
                                  index.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                title: Text(
                                  snapshot.data!.docs[index]['status'],
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: darkGrey),
                                ),
                                subtitle: Text(
                                    DateFormat.MMMd().add_jm().format(snapshot
                                        .data!.docs.first['time']
                                        .toDate()),
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: primaryColor, fontSize: 15)),
                              ),
                            ),
                          );
                        }),
                  );
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(120.0),
                    child: Center(
                        child: Column(
                      children: const [
                        Image(image: AssetImage('assets/no-orders.png')),
                        Text(
                          'No Orders',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17),
                        ),
                      ],
                    )),
                  );
                }
              }),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                constraints: BoxConstraints(
                  maxHeight: double.infinity,
                ),
                child: getSuggestContaier(
                    'Just For You', "", "", false, false, context)),
          )
        ],
      ),
    ));
  }
}
