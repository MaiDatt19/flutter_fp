<h1>Require</h1> 
Min Dart SDK version: 2.17.3 (stable)
<h1>Step to run</h1>
<ol>
<li>Clone repo to folder</li>
<li>Open with supported IDE/editors</li>
<li>Run 'flutter create .' on project root cmd/terminal</li>
<li>start run project</li>

<h1>Demo</h1>
<img src="https://drive.google.com/uc?id=1SGzr8sZ7Xxya7xJXJ3RgZvYmNHh9DfiR" width=200>
<img src="https://drive.google.com/uc?id=1wlxt2RznliwGh_S3I3sark_iaUTdit55" width=200>
<img src="https://drive.google.com/uc?id=13lkYSmKYNu27VtgOpiSR3_4MhWYoEXW6" width=200>
<img src="https://drive.google.com/uc?id=1Y2Frkt7kwJzooVDbv52pdrMQzcXo2382" width=200>


<!-- Contributors:
Doan Van Nghia (katzenundhundle): Backend, messeger screen, startup, sign in/up screens, user shop screen to post products screen, improve UI in product detail screen. 
Tran Hoai Phat, (201zinzin69): Product screen, place order, add shipping address, user screen to post add shipping address, nick Phạm Thị Bảo Hân cũng là em luôn ạ
Mai Le Tien Dat (MaiDatt19): Setting Screen, Cart Screen, Profile Screen, QR Scanner.
Dam Viet Cuong (vietcuongg): Following Screen, Recommend Screen, My Orders, Hot Topics.
-->



